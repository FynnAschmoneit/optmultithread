#optMultiThread
CC = g++
CFLAGS = -Wall -Wuninitialized -c -O2
LFLAGS = -Wall -Wuninitialized
OMP = -fopenmp

multiThreadOptOut : optMain.o
	$(CC) $(LFLAGS) $(OMP) optMain.cpp -o multiThreadOptOut
clean:
	rm -f core multiThreadOptOut optMain.o
