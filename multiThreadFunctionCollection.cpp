// multiThreadFunctionCollection
#include "preStreamingFunctions.cpp"		// only prestreaming for all 18 directions
#include "postStreamingFunctions.cpp"		// only prestreaming for all 18 directions

#include <fstream>
#include <cstring>			// for strtok()
#include "stdlib.h"			// for atoi()
#include "math.h"
using namespace std;		// for fstream

fltp p2(double in){
	return in*in;
}

void setParameters(){
	int streamsize = 200;
	char* streamIn = new char[streamsize];
	char* handOver = new char[streamsize];

	fstream parameters;
	parameters.open("parameters.txt");

   if(parameters == NULL){
    	printf("ERROR in setParameters: no input file found.\n");
    	getchar();
    }

    parameters.getline(streamIn,streamsize);		// not interesting line
    parameters.getline(streamIn,streamsize);
    handOver = strtok(streamIn,"$");
    //lenX,Y
    handOver = strtok(NULL,"$");
    lenX = atoi(handOver);
	handOver = strtok(NULL,"$");
    lenY = atoi(handOver);
    // midX,Y,radius, 
    parameters.getline(streamIn, streamsize);
    handOver = strtok(streamIn, "$");
    handOver = strtok(NULL, "$");
    midX = atoi(handOver);
    handOver = strtok(NULL, "$");
    midY = atoi(handOver);
    handOver = strtok(NULL, "$");
    radius = atoi(handOver);
    
    // barWidth, interface widt
    parameters.getline(streamIn, streamsize);
    handOver = strtok(streamIn, "$");
    handOver = strtok(NULL, "$");
	barWidth = atoi(handOver);
	handOver = strtok(NULL, "$");
    intWidth = atoi(handOver);
   // rho L,V
    parameters.getline(streamIn, streamsize);
    handOver = strtok(streamIn, "$");
    handOver = strtok(NULL, "$");
	rhoL = atof(handOver);
    handOver = strtok(NULL, "$");
	rhoV = atof(handOver);
	// debug coordinates
	parameters.getline(streamIn, streamsize);
    handOver = strtok(streamIn, "$");
    handOver = strtok(NULL, "$");
    observeX = atoi(handOver);
    handOver = strtok(NULL, "$");
    observeY = atoi(handOver);
    // velocity, Weber number, Reynolds number
	parameters.getline(streamIn, streamsize);
    handOver = strtok(streamIn, "$");
    handOver = strtok(NULL, "$");
    velocity = atof(handOver);
    handOver = strtok(NULL, "$");
    weberNum = atof(handOver);
    handOver = strtok(NULL, "$");
    reynoldsL = atof(handOver);
    // surface tension and kin visc
    parameters.getline(streamIn, streamsize);
    handOver = strtok(streamIn, "$");
    handOver = strtok(NULL, "$");
// printf("%s\n",handOver);
    srfcTens = atof(handOver);
    handOver = strtok(NULL, "$");
    tauLq = atof(handOver);
    handOver = strtok(NULL, "$");
    tauVp = atof(handOver);
    tauLq = tauLq/3.0;
    tauVp = tauVp/3.0;

    // itStop, itIntervall
    parameters.getline(streamIn, streamsize);
    handOver = strtok(streamIn, "$");
    handOver = strtok(NULL, "$");
    itStop = atoi(handOver);
    handOver = strtok(NULL, "$");
    itIntervall = atoi(handOver);
    // numthreads
    parameters.getline(streamIn, streamsize);
    handOver = strtok(streamIn, "$");
    handOver = strtok(NULL, "$");
    numThreads = atoi(handOver);

    lenAr = lenX*lenY;
	// srfcTens = 2.0*rhoL*velocity*velocity*radius/weberNum;        
    beta = 12.0*srfcTens/(p2(p2(rhoL - rhoV)) * intWidth);   // interaction amplitude
    kappa = 1.5*intWidth*srfcTens/p2(rhoL - rhoV);
	// tauLq = 6.0* velocity * radius / reynoldsL;
    // tauVp = velocity * radius * 3.0/10.0;

    itStep = 0;
    itBreak = itIntervall;

    printf("initial parameters:\n\
    	grid:\t\tlenX = %d, lenY = %d\n\
    	bubble\t\tmidX = %d, midY = %d, radius = %d\n\
    	bar\t\tBarwidth = %d, interface width = %d\n\
    	density\t\trhoLq = %.1f, rhoVp = %.1f\n\
    	debug\t\tobserveX = %d, observeY = %d\n\
    	velocity = %.5f\n\tWeber number = %.1f\n\tReynolds number = %.1f\n\
    	surface tension = %.6f\n\tbeta = %.10f\n\tkappa = %.10f\n\ttauLq = %f, tauVp = %f\n\
    	iteration stop = %d, data export intervall = %d\n"
    	,lenX,lenY,midX,midY,radius,barWidth,intWidth,rhoL,rhoV,observeX,observeY, velocity,
    	 weberNum, reynoldsL, srfcTens, beta, kappa, tauLq, tauVp,itStop,itIntervall);

    parameters.close();
}

void setBubble(int midX, int midY, int rad, int width, double velocity){
  double rhoAmpl;
  double dstc;
  int i,j;
  for ( int b = 0; b<lenAr ;b++){
  	i = b%lenX;
  	j = b/lenX;
	dstc = sqrt( p2(double(i-midX)) + p2(double(j-midY)) );
	rhoAmpl = 0.5*(rhoL+rhoV) + 0.5*(rhoL-rhoV)*tanh( 2.0/double(width) * ( rad - dstc)) ; 
	if( rhoAmpl > rho[i + j*lenX ] ){
	  rho[i + j*lenX ] = rhoAmpl;
	}
	// if( dstc <= rad + width+0.5){
	//   u[0] = 0.0;
	//   u[1] = 0.0;
	//   // nd[i][j].u[1] = -velocity;
	// }
  }
}

void macroscopic(bool macParallel){			// all quantities here must be global, not threadprivate!
	fltp dXX,dYY,dXY,Dx,Dy;
	fltp inv3 = 1.0/3.0;
	fltp inv6 = 1.0/6.0;
	fltp inv12 = 1.0/12.0;
	fltp rhoM = 0.5*(rhoL + rhoV);
	#pragma omp parallel if(macParallel)
	{
	#pragma omp for 
	for (int i = 0; i < lenAr; i++){
		rho[i] = f_0[i] + f_1[i] + f_2[i] + f_3[i] + f_4[i] + f_5[i] + f_6[i] + f_7[i] + f_8[i];
	}

	#pragma omp for
	for (int i=0; i<lenAr; i++){
		dx[i] = inv12*(	4.0*( rho[nn1[i]] - rho[nn3[i]] ) + rho[nn5[i]]	- rho[nn6[i]] - rho[nn7[i]] + rho[nn8[i]] );
		dy[i] = inv12*(	4.0*( rho[nn2[i]] - rho[nn4[i]] ) + rho[nn5[i]]	+ rho[nn6[i]] - rho[nn7[i]] - rho[nn8[i]] );
		
		lapRho[i] = inv6*(	4.0*( rho[nn_1[i]] +rho[nn_2[i]] +rho[nn_3[i]] +rho[nn_4[i]] ) +rho[nn_5[i]] +rho[nn_6[i]] +rho[nn_7[i]] +rho[nn_8[i]] -20.0*rho[i]	);
		B_[i] = dx[i]*dx[i] +dy[i]*dy[i];
		psi[i] = beta*4.0*(rho[i] - rhoL)*(rho[i] - rhoV)*(rho[i] - rhoM) - kappa*lapRho[i] ;
	}		

	#pragma omp for
	for(int i=0; i<lenAr; i++){
		dXX = inv12*(	4.0*( dx[nn1[i]] - dx[nn3[i]] ) + dx[nn5[i]]	- dx[nn6[i]] - dx[nn7[i]] + dx[nn8[i]] );
		dYY = inv12*(	4.0*( dy[nn2[i]] - dy[nn4[i]] ) + dy[nn5[i]]	+ dy[nn6[i]] - dy[nn7[i]] - dy[nn8[i]] );
		dXY = inv12*(	4.0*( dx[nn2[i]] - dx[nn4[i]] ) + dx[nn5[i]]	+ dx[nn6[i]] - dx[nn7[i]] - dx[nn8[i]] );

		Dx = 1.5*kappa*(	dy[i]*dXY - dx[i]*dYY);
		Dy = 1.5*kappa*(	dx[i]*dXY - dy[i]*dXX);

		u_1[i] = (g_1[i] - g_3[i] + g_5[i] - g_6[i] - g_7[i] + g_8[i] + inv3*Dx )/rho[i];
		u_2[i] = (g_2[i] - g_4[i] + g_5[i] + g_6[i] - g_7[i] - g_8[i] + inv3*Dy +0.5*rho[i]*gravConst )/rho[i];
		
		Du[i] = Dx*u_1[i] + Dy*u_2[i];
		dxPsi[i] = inv12*(	4.0*( psi[nn1[i]] - psi[nn3[i]] ) + psi[nn5[i]]	- psi[nn6[i]] - psi[nn7[i]] + psi[nn8[i]] );
		dyPsi[i] = inv12*(	4.0*( psi[nn2[i]] - psi[nn4[i]] ) + psi[nn5[i]]	+ psi[nn6[i]] - psi[nn7[i]] - psi[nn8[i]] );
		prssr[i] = inv3*( g_0[i] + g_1[i] + g_2[i] + g_3[i] + g_4[i] + g_5[i] + g_6[i] + g_7[i] + g_8[i]  + 0.5*( dx[i]*u_1[i] + dy[i]*u_2[i] ) );
	}	
	}
}


void initialize(){
	setParameters();
	rho = new fltp[lenAr];
	u_1 = new fltp[lenAr];
	u_2 = new fltp[lenAr];
	prssr = new fltp[lenAr];
	psi = new fltp[lenAr];

	g_0 = new fltp[lenAr];
	g_1 = new fltp[lenAr];
	g_2 = new fltp[lenAr];
	g_3 = new fltp[lenAr];
	g_4 = new fltp[lenAr];
	g_5 = new fltp[lenAr];
	g_6 = new fltp[lenAr];
	g_7 = new fltp[lenAr];
	g_8 = new fltp[lenAr];

	f_0 = new fltp[lenAr];
	f_1 = new fltp[lenAr];
	f_2 = new fltp[lenAr];
	f_3 = new fltp[lenAr];
	f_4 = new fltp[lenAr];
	f_5 = new fltp[lenAr];
	f_6 = new fltp[lenAr];
	f_7 = new fltp[lenAr];
	f_8 = new fltp[lenAr];

	nn_1 = new int[lenAr];
	nn_2 = new int[lenAr];
	nn_3 = new int[lenAr];
	nn_4 = new int[lenAr];
	nn_5 = new int[lenAr];
	nn_6 = new int[lenAr];
	nn_7 = new int[lenAr];
	nn_8 = new int[lenAr];

	Du = new fltp[lenAr];
	dx = new fltp[lenAr];
	dy = new fltp[lenAr];
	dxPsi = new fltp[lenAr];
	dyPsi = new fltp[lenAr];
	lapRho = new fltp[lenAr];
	B_ = new fltp[lenAr];
	
	if(!parallel){	
		numThreads = 1;
	}
	omp_set_num_threads(numThreads);

	relTauRhoRatio = (tauLq - tauVp)/(rhoL - rhoV); 
	Gconst = gravConst/24.0;

	for (int i = 0; i < lenAr; ++i){
		rho[i] = rhoV;
		u_1[i] = 0.0;
		u_2[i] = 0.0;
		prssr[i] = 1.0;
	}
	setBubble(midX,midY,radius,intWidth,velocity);

	// int x,y,xp,xm,yp,ym;
	for(int i =0; i<lenAr; i++){
		f_0[i] = w1*rho[i];
		f_1[i] = w2*rho[i];
		f_2[i] = w2*rho[i];
		f_3[i] = w2*rho[i];
		f_4[i] = w2*rho[i];
		f_5[i] = w3*rho[i];
		f_6[i] = w3*rho[i];
		f_7[i] = w3*rho[i];
		f_8[i] = w3*rho[i];

		g_0[i] = w1*3.0*prssr[i];
		g_1[i] = w2*3.0*prssr[i];
		g_2[i] = w2*3.0*prssr[i];
		g_3[i] = w2*3.0*prssr[i];
		g_4[i] = w2*3.0*prssr[i];
		g_5[i] = w3*3.0*prssr[i];
		g_6[i] = w3*3.0*prssr[i];
		g_7[i] = w3*3.0*prssr[i];
		g_8[i] = w3*3.0*prssr[i];

		// x = i%lenX;
		// y = i/lenX;
		// xp = (i+1)%lenX;
		// xm = (i-1)%lenX;	if( i==0 ){ xm = lenX-1; }
		// yp = (i/lenX + 1) * ( 1 - (i+lenX)/(lenX*lenY));
		// ym = ( i/lenX -1); if( i/lenX == 0){ ym += lenY; } 

		// nn_1[i] = xp + y*lenX;
		// nn_2[i] = x + yp*lenX;
		// nn_3[i] = xm + y*lenX;
		// nn_4[i] = x + ym*lenX;
		// nn_5[i] = xp + yp*lenX;
		// nn_6[i] = xm + yp*lenX;
		// nn_7[i] = xm + ym*lenX;
		// nn_8[i] = xp + ym*lenX;

		nn_1[i] = (i+1)%lenX + (i/lenX)*lenX;
		nn_2[i] = i%lenX + ((i+lenX)/lenX)%lenY*lenX;
		nn_3[i] = (i-1+lenX)%lenX + (i/lenX)*lenX;
		nn_4[i] = i%lenX + ((i-lenX) + lenAr)%lenAr/lenY*lenX;
		nn_5[i] = (nn_2[i]+1)%lenX + (nn_2[i]/lenX)*lenX;
		nn_6[i] = (nn_2[i]-1+lenX)%lenX + (nn_2[i]/lenX)*lenX;	
		nn_7[i] = (nn_4[i]-1+lenX)%lenX + (nn_4[i]/lenX)*lenX;	
		nn_8[i] = (nn_4[i]+1)%lenX + (nn_4[i]/lenX)*lenX;	
	}

	#pragma omp parallel
	{
	nn1 = new int[lenAr];
	nn2 = new int[lenAr];
	nn3 = new int[lenAr];
	nn4 = new int[lenAr];
	nn5 = new int[lenAr];
	nn6 = new int[lenAr];
	nn7 = new int[lenAr];
	nn8 = new int[lenAr];

	arrayCopyInt(nn1,nn_1,lenAr);
	arrayCopyInt(nn2,nn_2,lenAr);
	arrayCopyInt(nn3,nn_3,lenAr);
	arrayCopyInt(nn4,nn_4,lenAr);
	arrayCopyInt(nn5,nn_5,lenAr);
	arrayCopyInt(nn6,nn_6,lenAr);
	arrayCopyInt(nn7,nn_7,lenAr);
	arrayCopyInt(nn8,nn_8,lenAr);

	// these are not changed by the algorithm. but every thread gets its copy
	nnP = new int[lenAr];
	nnM = new int[lenAr];

	distr_Loc = new fltp[lenAr];

//macrc	
	rho_LOC = new fltp[lenAr]; 
	u_1_LOC = new fltp[lenAr];
	u_2_LOC = new fltp[lenAr];
	prssr_LOC = new fltp[lenAr];
	psi_LOC = new fltp[lenAr];
	B_LOC = new fltp[lenAr];
	dx_LOC = new fltp[lenAr];
	dy_LOC = new fltp[lenAr];
	dxPsi_LOC = new fltp[lenAr];
	dyPsi_LOC = new fltp[lenAr];
	Du_LOC = new fltp[lenAr];
	lapRho_LOC = new fltp[lenAr];
	}	// end of parallel
	
	macroscopic( false );
}

void preCollStream(){

	// firstprivate are private to every thread and already initialised.
	#pragma omp parallel firstprivate(lenAr,lenX,lenY,inv12,kappa,w1,w2,w3,Gconst,gravConst ) 
	{		
//---------------------------- local copies of macroscopic quatities
	arrayCopy(rho_LOC,rho,lenAr);
	arrayCopy(u_1_LOC,u_1,lenAr);
	arrayCopy(u_2_LOC,u_2,lenAr);
	arrayCopy(prssr_LOC,prssr,lenAr);
	arrayCopy(psi_LOC,psi,lenAr);
	arrayCopy(dx_LOC, dx, lenAr);
	arrayCopy(dy_LOC, dy, lenAr);
	arrayCopy(B_LOC, B_, lenAr);
	arrayCopy(dxPsi_LOC, dxPsi, lenAr);
	arrayCopy(dyPsi_LOC, dyPsi, lenAr);
	arrayCopy(lapRho_LOC, lapRho, lenAr);
	arrayCopy(Du_LOC, Du, lenAr);
//------------------------------starting pre-streaming collision and streaming
	//--------momentum fied:
	#pragma omp single nowait
		preStr_G0();

	#pragma omp single nowait
		preStr_G1();

	#pragma omp single nowait
		preStr_G2();

	#pragma omp single nowait
		preStr_G3();

	#pragma omp single nowait
		preStr_G4();

	#pragma omp single nowait
		preStr_G5();

	#pragma omp single nowait
		preStr_G6();

	#pragma omp single nowait
		preStr_G7();

	#pragma omp single nowait
		preStr_G8();

	//------order parameter field:
	#pragma omp single nowait
		preStr_F0();

	#pragma omp single nowait
		preStr_F1();

	#pragma omp single nowait
		preStr_F2();

	#pragma omp single nowait
		preStr_F3();

	#pragma omp single nowait
		preStr_F4();

	#pragma omp single nowait
		preStr_F5();

	#pragma omp single nowait
		preStr_F6();

	#pragma omp single nowait
		preStr_F7();

	#pragma omp single 
		preStr_F8();
	}
}

void postCollStream(){
	#pragma omp parallel firstprivate(lenAr,lenX,lenY,inv12,kappa,w1,w2,w3,Gconst,gravConst ) 
	{		

//---------------------------- local copies of macroscopic quatities
	arrayCopy(rho_LOC,rho,lenAr);
	arrayCopy(u_1_LOC,u_1,lenAr);
	arrayCopy(u_2_LOC,u_2,lenAr);
	arrayCopy(prssr_LOC,prssr,lenAr);
	arrayCopy(psi_LOC,psi,lenAr);
	arrayCopy(dx_LOC, dx, lenAr);
	arrayCopy(dy_LOC, dy, lenAr);
	arrayCopy(B_LOC, B_, lenAr);
	arrayCopy(dxPsi_LOC, dxPsi, lenAr);
	arrayCopy(dyPsi_LOC, dyPsi, lenAr);
	arrayCopy(lapRho_LOC, lapRho, lenAr);
	arrayCopy(Du_LOC, Du, lenAr);


	#pragma omp single nowait
		postStr_G0();

	#pragma omp single nowait
		postStr_G1();

	#pragma omp single nowait
		postStr_G2();

	#pragma omp single nowait
		postStr_G3();

	#pragma omp single nowait
		postStr_G4();

	#pragma omp single nowait
		postStr_G5();

	#pragma omp single nowait
		postStr_G6();

	#pragma omp single nowait
		postStr_G7();

	#pragma omp single nowait
		postStr_G8();

	//------order parameter field:
	#pragma omp single nowait
		postStr_F0();

	#pragma omp single nowait
		postStr_F1();

	#pragma omp single nowait
		postStr_F2();

	#pragma omp single nowait
		postStr_F3();

	#pragma omp single nowait
		postStr_F4();

	#pragma omp single nowait
		postStr_F5();

	#pragma omp single nowait
		postStr_F6();

	#pragma omp single nowait
		postStr_F7();

	#pragma omp single 
		postStr_F8();
	}
}



void gnuPlotPrint(){	
	int i,j;
	double alpha;
	FILE *gnuPlotFile;
	gnuPlotFile = fopen("densityMapFinal.csv","w");

	// for ( int b=0; b<10*lenX; b++){
	for ( int b=0; b<lenAr; b++){
		i = b%lenX;
		j = b/lenX;
		alpha = rho[b];
		fprintf(gnuPlotFile,"%d \t %d \t %.10f \n",j,i, alpha);
	} 

	fclose(gnuPlotFile);

	FILE *gp;
	gp = popen("gnuplot -persist","w");
	fprintf(gp, "set terminal x11 0\n");
	fprintf(gp, "plot \"densityMapFinal.csv\" using 2:1:3 with image\n");
	fclose(gp);
}

void dump(int x, int y){
	int b = y*lenX+x;

	printf("\ndump node: x=%d, y=%d\t\tpos in array=%d\n",x,y,b);
	printf("\trho = %.8f\n",rho[b]);
	printf("\tprssr = %.8f\n",prssr[b]);
	printf("\tu_1 = %f, u_2 = %.8f\n",u_1[b],u_2[b]);
	printf("\tdx = %f, dy = %.8f\n",dx[b],dy[b]);

	printf("\tnn_i = (%d, %d,  %d, %d, %d, %d, %d, %d )\n",nn_1[b],nn_2[b],nn_3[b],nn_4[b],nn_5[b],nn_6[b],nn_7[b],nn_8[b]);
	printf("\tg_i = ( %.3f, %.3f, %.3f, %.3f, %.3f, %.3f, %.3f, %.3f, %.3f )\n",g_0[b],g_1[b],g_2[b],g_3[b],g_4[b],g_5[b],g_6[b],g_7[b],g_8[b]);
	printf("\tf_i = ( %.3f, %.3f, %.3f, %.3f, %.3f, %.3f, %.3f, %.3f, %.3f )\n",f_0[b],f_1[b],f_2[b],f_3[b],f_4[b],f_5[b],f_6[b],f_7[b],f_8[b]);
}


void deleteDynamicArrays(){
	#pragma omp parallel
	{
	// all these arrays are copied to every thread:
	// #pragma omp threadprivate(nn1,nn2,nn3,nn4,nn5,nn6,nn7,nn8,nnP,nnM,drv_XX,drv_YY,drv_XY,Dx,Dy)
	delete [] nn1;
	delete [] nn2;
	delete [] nn3;
	delete [] nn4;
	delete [] nn5;
	delete [] nn6;
	delete [] nn7;
	delete [] nn8;
	delete [] nnP;
	delete [] nnM;
	delete [] rho_LOC;
	delete [] distr_Loc;
	delete [] u_1_LOC;
	delete [] u_2_LOC;
	delete [] prssr_LOC;
	delete [] psi_LOC;
	}
}

int abort( int it){
	for ( int b=0; b<lenAr; b++){
    if (rho[b] != rho[b]){
		// if (rho[10] != rho[10]){
			// printf("NAN found. iteration stopped at step %d\n",it);
			printf("NAN found in node %d (%d,%d). iteration stopped at step %d\n",b,b%lenX,b/lenX,it);
			return 0;
		}
	}
	return -1;
}

double pressureDiff_OMP(){
	int distc,i,j,nodesIn,nodesOut;
    double pIn,pOut;
    pIn = 0.0;
    pOut = 0.0;
    nodesIn = 0;
    nodesOut = 0;
    // #pragma omp for
    for(int b=0; b<lenAr; b++){
        i = b%lenX;
        j = b/lenX;
        distc = sqrt( p2(i-midX) + p2(j-midY));
        if( distc < radius-intWidth ){
            pIn += prssr[b];
            nodesIn++;
        } else if(distc > radius + intWidth ){
            pOut += prssr[b];
            nodesOut++;
        }
    }
    pIn = pIn/double(nodesIn);
    pOut = pOut/double(nodesOut);
    return pIn - pOut;
}

double averagePressDiff(double* convArray){
	double dummy = 0.0;
	for(int i=0; i<itIntervall; i++){
		dummy += convArray[i];
	}
	dummy =  dummy/double(itIntervall);
	double relError = (dummy - srfcTens/double(radius))*double(radius)/srfcTens;
	printf("mean pressre Difference: %.10f, relative error from theory: %.10f \n",dummy,relError);
	return relError;
}

void exportArray(char* filename,double* errorArray,int len){
  char handover[100];
	const char* macr = "_error.csv";
    strcpy(handover,filename);
    strcat(handover,macr);
    printf("writing in: %s\n",handover);
	FILE *errors = fopen(handover,"w");
    for(int b=0; b<len; b++){
      fprintf(errors,"%.10f, ",errorArray[b]);    
    }
    fclose(errors); 
}


void exportData(char* filename, bool density, bool pressure, bool velx, bool vely){
  char handover[100];
  
  if(density){
    const char* macr = "_Rho.csv";
    strcpy(handover,filename);
    strcat(handover,macr);
    printf("writing in: %s\n",handover);
    FILE *rhoField = fopen(handover,"a");
    for(int b=0; b<lenAr-1; b++){
      fprintf(rhoField,"%.10f, ",rho[b]);    
    }
    fprintf(rhoField,"%.10f\n",rho[lenAr-1]);   
    fclose(rhoField); 
  }

  if(pressure){
    const char* macr = "_Prssr.csv";
    strcpy(handover,filename);
    strcat(handover,macr);
    printf("writing in: %s\n",handover);
    FILE *prssrField = fopen(handover,"a");
    for(int b=0; b<lenAr-1; b++){
      fprintf(prssrField,"%.10f, ",prssr[b]);    
    }
    fprintf(prssrField,"%.10f\n",prssr[lenAr-1]);   
    fclose(prssrField); 
  }

  if(velx){
    const char* macr = "_VelX.csv";
    strcpy(handover,filename);
    strcat(handover,macr);
    printf("writing in: %s\n",handover);
    FILE *velXField = fopen(handover,"a");
    for(int b=0; b<lenAr-1; b++){
      fprintf(velXField,"%.10f, ",u_1[b]);    
    }
    fprintf(velXField,"%.10f\n",u_1[lenAr-1]);   
    fclose(velXField); 
  }

  if(vely){
    const char* macr = "_VelY.csv";
    strcpy(handover,filename);
    strcat(handover,macr);
    printf("writing in: %s\n",handover);
    FILE *velYField = fopen(handover,"a");
    for(int b=0; b<lenAr-1; b++){
      fprintf(velYField,"%.10f, ",u_2[b]);    
    }
    fprintf(velYField,"%.10f\n",u_2[lenAr-1]);   
    fclose(velYField); 
  }
}







