// optMain.cpp
#include <cstdio>
#include "omp.h"
#define fltp double
#define lenConvAr 400

#include "auxiliary.cpp"
#include "threadPublicVariables.cpp"
#include "multiThreadFunctionCollection.cpp"


int main(){
	bool gnuPlot = true;
    bool rhoPlot = true;
    bool prssrPlot = true;
    bool uxPlot = true;
    bool uyPlot = true;
    bool exportDataSet = false;

	printf("\n\n\n\t\t\tHallo, ich bin optMain.\n\n\n");	

	initialize();
	int counter = 0;
	int counter2 = 0;
	double convArray[lenConvAr];
	double errorArray[itStop/itIntervall];
	// double *convArray = new double[itIntervall];
	double passedTime = omp_get_wtime();
	double meanPressDiff;
	double PressDiffRelError;
	double PressDiffRelError2 = 1000.0;
	bool converged = false;
	// dump(observeX,observeY);
	
	printf("\n\nstarting simulation using %d thread(s).\n\n",numThreads);
	while (itStep < itStop && converged == false){
	    while (itStep < itBreak && itStep < itStop){
			preCollStream();
			macroscopic(true);
			postCollStream();
			itStep++;
		    convArray[counter] = pressureDiff_OMP();
			counter++;
		}
		dump(observeX,observeY);

		counter = 0;
		meanPressDiff = averagePressDiff(convArray);
		errorArray[counter2] = meanPressDiff;
		counter2++;
		// PressDiffRelError = (meanPressDiff-srfcTens/double(radius))*double(radius)/srfcTens;
		// if( (PressDiffRelError - PressDiffRelError2)*(PressDiffRelError - PressDiffRelError2) < 0.00000001){	// if the difference of two successive meanPrssDiff is smaller than 0.1%
		// 	converged = true;
		// }
		// PressDiffRelError2 = PressDiffRelError;

		if(abort(itStep) == 0){	return 1;	}
	    printf("progress:\t%.1f%%\n",double(itStep)/itStop*100);
	    itBreak += itIntervall;
	}
	passedTime = omp_get_wtime() - passedTime;
	printf("time for %d iterations, using %d thread(s): %f\n",itStop,numThreads,passedTime);
	printf("pressure discontinuity = %f\n",meanPressDiff);
	deleteDynamicArrays();

	if (gnuPlot){
		gnuPlotPrint();
	}
	if (exportDataSet){
		exportArray("errorArray",errorArray,itStop/itIntervall);
		exportData("laplaceOut", rhoPlot, prssrPlot, uxPlot, uyPlot);
	}



	return 0;
}