// openMpTest.cpp

#include <cstdio>
#include <stdio.h>
#include <omp.h>
#include <string.h>

#define N 100
#define integer int

integer *array, *array_private;
#pragma omp threadprivate(array_private)



extern integer* alpha;
void function1(){
	omp_set_num_threads(2);
	#pragma omp parallel firstprivate(alpha)
    {
        array_private = new integer[N];
        memcpy(array_private, array, sizeof(int)*N); //or use std::copy
        for (integer j=0; j<N; j++){
        	array_private[j] += omp_get_thread_num();
        }     
    }
}

// extern int alpha;
void function2(){
	#pragma omp parallel
    {	
    	printf("entry 5 in array_private = %d, from thead %d and alpha = %d\n",array_private[5],omp_get_thread_num(),alpha[2]);
    }
}

void function3(){
    delete[] array_private;
}
integer* alpha = new integer[10];

integer main() {
	alpha[2] = 2;

    array = new integer[N];
    for (integer i=0; i<N; i++){
    	array[i] = i;
    }

    function1();
    function2();
    function2();
    function2();
    function3();

    delete[] array;
}