// threadPublicVariables

int lenX, lenY, lenAr, midX, midY, radius, barWidth, intWidth, observeX, observeY, itStep, itStop, itIntervall, itBreak, numThreads;

fltp rhoL, rhoV, weberNum, reynoldsL, velocity, beta, kappa, srfcTens, tauLq, tauVp, relTauRhoRatio, Gconst;
fltp gravConst = 0.00000;

fltp w1 = 4.0/9.0;
fltp w2 = 1.0/9.0;
fltp w3 = 1.0/36.0;
fltp inv12 = 1.0/12.0;

bool parallel = true;



int *nn_1,*nn_2,*nn_3,*nn_4,*nn_5,*nn_6,*nn_7,*nn_8; // pointers to nearest neighbours:

// global arrays, no copies to threads. These get updated in macroscopic()
fltp *rho, *u_1, *u_2, *prssr, *psi, *B_, *Du, *dx, *dy, *dxPsi, *dyPsi, *lapRho;

// local copy distr_Loc for respective collision function
fltp *g_0,*g_1,*g_2,*g_3,*g_4,*g_5,*g_6,*g_7,*g_8;
fltp *f_0,*f_1,*f_2,*f_3,*f_4,*f_5,*f_6,*f_7,*f_8;

// pointers to threadlocal cooies of nearest neighbours: These arrays may be changed, but they are not used outside their respective thread. They need to be copied before a thread uses them.
int *nn1, *nn2, *nn3, *nn4, *nn5, *nn6, *nn7, *nn8, *nnP, *nnM;			// copied in initialize()
fltp *distr_Loc, *rho_LOC, *u_1_LOC, *u_2_LOC, *prssr_LOC, *psi_LOC, *B_LOC, *dx_LOC, *dy_LOC, *dxPsi_LOC, *dyPsi_LOC, *Du_LOC, *lapRho_LOC;

#pragma omp threadprivate(nn1,nn2,nn3,nn4,nn5,nn6,nn7,nn8,nnP,nnM,distr_Loc,rho_LOC,u_1_LOC,u_2_LOC,prssr_LOC,psi_LOC,B_LOC,dx_LOC,dy_LOC,dxPsi_LOC,dyPsi_LOC,Du_LOC,lapRho_LOC)




