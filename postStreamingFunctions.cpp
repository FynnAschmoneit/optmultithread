// postStreamingFunctions.cpp

void postStr_G0(){
	arrayCopy(distr_Loc, g_0, lenAr);

	fltp weight = w1;
	
	fltp gEQ,gamma,gamAux,u1,u2,degRate,EU,G,tau,rhoLc;
	
	for (int k=0; k<lenAr; k++){

		rhoLc = rho_LOC[k];
		u1 = u_1_LOC[k];
		u2 = u_2_LOC[k];
		gamAux = weight * ( - 1.5*(u1*u1 + u2*u2 ) );
		gamma = weight + gamAux;
		gEQ = 3.0*prssr_LOC[k]*weight + rhoLc*gamAux;
		tau = tauVp + (rhoLc - rhoV)*relTauRhoRatio;
		degRate = 1.0/(2.0*tau + 1.0);
		EU = dx_LOC[k]*u1+dy_LOC[k]*u2;
		G = -Gconst*16.0*rhoLc*( u2-0.5*gravConst );

		g_0[k] = g_0[k] + degRate*( gEQ - g_0[k] +2.0*tau*( - Du_LOC[k]*gamma -0.5*EU*gamAux + G )			);
	}	
}

void postStr_G1(){
	arrayCopy(distr_Loc, g_1, lenAr);
	nnP = nn_1;
	nnM = nn_3;	

	fltp weight = w2;
	
	fltp gEQ,gamma,gamAux,u1,u2,degRate,EU,dirDrv,dirDrvB,dirDrvC_x,dirDrvC_y,eD,G,tau,rhoLc;

	for (int k=0; k<lenAr; k++){

		rhoLc = rho_LOC[k];
		u1 = u_1_LOC[k];
		u2 = u_2_LOC[k];
		gamAux = weight * ( - 1.5*(u1*u1 + u2*u2 ) + 3.0*u1 + 4.5*u1*u1 );
		gamma = weight + gamAux;
		gEQ = 3.0*prssr_LOC[k]*weight + rhoLc*gamAux;
		tau = tauVp + (rhoLc - rhoV)*relTauRhoRatio;
		degRate = 1.0/(2.0*tau + 1.0);
		dirDrv = 0.5*( rho_LOC[nnP[k]] - rho_LOC[nnM[k]]);
		
		dirDrvB = 0.5*( B_LOC[nnP[k]] - B_LOC[nnM[k]] );
		dirDrvC_x = 0.5*( dx_LOC[nnP[k]] - dx_LOC[nnM[k]] );
		dirDrvC_y = 0.5*( dy_LOC[nnP[k]] - dy_LOC[nnM[k]] );
		eD = 1.5*kappa*(dirDrvB - dirDrv*lapRho_LOC[k] - dx_LOC[k]*dirDrvC_x - dy_LOC[k]*dirDrvC_y);

		EU = dx_LOC[k]*u1+dy_LOC[k]*u2;
		G = Gconst*4.0*rhoLc*( 3.0*u1 - u2 - 0.5*gravConst );

		g_1[k] = g_1[k] + degRate*( gEQ - g_1[k] +2.0*tau*( (eD - Du_LOC[k])*gamma +0.5*( dirDrv - EU )*gamAux + G )			);
	}
}

void postStr_G2(){
	arrayCopy(distr_Loc, g_2, lenAr);
	nnP = nn_2;
	nnM = nn_4;

	fltp weight = w2;
	
	fltp gEQ,gamma,gamAux,u1,u2,degRate,EU,dirDrv,dirDrvB,dirDrvC_x,dirDrvC_y,eD,G,tau,rhoLc;

	for (int k=0; k<lenAr; k++){
		
		rhoLc = rho_LOC[k];
		u1 = u_1_LOC[k];
		u2 = u_2_LOC[k];
		gamAux = weight * ( - 1.5*(u1*u1 + u2*u2 )  + 3.0*u2 + 4.5*u2*u2 );
		gamma = weight + gamAux;
		gEQ = 3.0*prssr_LOC[k]*weight + rhoLc*gamAux;
		tau = tauVp + (rhoLc - rhoV)*relTauRhoRatio;
		degRate = 1.0/(2.0*tau + 1.0);
		dirDrv = 0.5*( rho_LOC[nnP[k]] - rho_LOC[nnM[k]]);
		
		dirDrvB = 0.5*( B_LOC[nnP[k]] - B_LOC[nnM[k]] );
		dirDrvC_x = 0.5*( dx_LOC[nnP[k]] - dx_LOC[nnM[k]] );
		dirDrvC_y = 0.5*( dy_LOC[nnP[k]] - dy_LOC[nnM[k]] );
		eD = 1.5*kappa*(dirDrvB - dirDrv*lapRho_LOC[k] - dx_LOC[k]*dirDrvC_x - dy_LOC[k]*dirDrvC_y);

		EU = dx_LOC[k]*u1+dy_LOC[k]*u2;
		G = Gconst*4.0*rhoLc*( 1.0+2.0*u2 + gravConst );

		g_2[k] = g_2[k] + degRate*( gEQ - g_2[k] +2.0*tau*( (eD - Du_LOC[k])*gamma +0.5*( dirDrv - EU )*gamAux + G )			);
	}
}

void postStr_G3(){
	arrayCopy(distr_Loc, g_3, lenAr);
	nnP = nn_3;
	nnM = nn_1;

	fltp weight = w2;
	
	fltp gEQ,gamma,gamAux,u1,u2,degRate,EU,dirDrv,dirDrvB,dirDrvC_x,dirDrvC_y,eD,G,tau,rhoLc;

	for (int k=0; k<lenAr; k++){

		rhoLc = rho_LOC[k];
		u1 = u_1_LOC[k];
		u2 = u_2_LOC[k];
		gamAux = weight * ( - 1.5*(u1*u1 + u2*u2 ) -3.0*u1 + 4.5*u1*u1 );
		gamma = weight + gamAux;
		gEQ = 3.0*prssr_LOC[k]*weight + rhoLc*gamAux;
		tau = tauVp + (rhoLc - rhoV)*relTauRhoRatio;
		degRate = 1.0/(2.0*tau + 1.0);
		dirDrv = 0.5*( rho_LOC[nnP[k]] - rho_LOC[nnM[k]]);
		
		dirDrvB = 0.5*( B_LOC[nnP[k]] - B_LOC[nnM[k]] );
		dirDrvC_x = 0.5*( dx_LOC[nnP[k]] - dx_LOC[nnM[k]] );
		dirDrvC_y = 0.5*( dy_LOC[nnP[k]] - dy_LOC[nnM[k]] );
		eD = 1.5*kappa*(dirDrvB - dirDrv*lapRho_LOC[k] - dx_LOC[k]*dirDrvC_x - dy_LOC[k]*dirDrvC_y);

		EU = dx_LOC[k]*u1+dy_LOC[k]*u2;
		G = -Gconst*4.0*rhoLc*( 3.0*u1 + u2 + 0.5*gravConst );

		g_3[k] = g_3[k] + degRate*( gEQ - g_3[k] +2.0*tau*( (eD - Du_LOC[k])*gamma +0.5*( dirDrv - EU )*gamAux + G )			);
	}
}

void postStr_G4(){
	arrayCopy(distr_Loc, g_4, lenAr);
	nnP = nn_4;
	nnM = nn_2;

	fltp weight = w2;
	
	fltp gEQ,gamma,gamAux,u1,u2,degRate,EU,dirDrv,dirDrvB,dirDrvC_x,dirDrvC_y,eD,G,tau,rhoLc;

	for (int k=0; k<lenAr; k++){
	
		rhoLc = rho_LOC[k];
		u1 = u_1_LOC[k];
		u2 = u_2_LOC[k];
		gamAux = weight * ( - 1.5*(u1*u1 + u2*u2 ) -3.0*u2 + 4.5*u2*u2 );
		gamma = weight + gamAux;
		gEQ = 3.0*prssr_LOC[k]*weight + rhoLc*gamAux;
		tau = tauVp + (rhoLc - rhoV)*relTauRhoRatio;
		degRate = 1.0/(2.0*tau + 1.0);
		dirDrv = 0.5*( rho_LOC[nnP[k]] - rho_LOC[nnM[k]]);
		
		dirDrvB = 0.5*( B_LOC[nnP[k]] - B_LOC[nnM[k]] );
		dirDrvC_x = 0.5*( dx_LOC[nnP[k]] - dx_LOC[nnM[k]] );
		dirDrvC_y = 0.5*( dy_LOC[nnP[k]] - dy_LOC[nnM[k]] );
		eD = 1.5*kappa*(dirDrvB - dirDrv*lapRho_LOC[k] - dx_LOC[k]*dirDrvC_x - dy_LOC[k]*dirDrvC_y);

		EU = dx_LOC[k]*u1+dy_LOC[k]*u2;
		G = -Gconst*4.0*rhoLc*( 1.0+4.0*u2-gravConst );

		g_4[k] = g_4[k] + degRate*( gEQ - g_4[k] +2.0*tau*( (eD - Du_LOC[k])*gamma +0.5*( dirDrv - EU )*gamAux + G )			);
	}
}

void postStr_G5(){
	arrayCopy(distr_Loc, g_5, lenAr);
	nnP = nn_5;
	nnM = nn_7;

	fltp weight = w3;
	
	fltp gEQ,gamma,gamAux,u1,u2,degRate,EU,dirDrv,dirDrvB,dirDrvC_x,dirDrvC_y,eD,G,tau,rhoLc;

	for (int k=0; k<lenAr; k++){
		
		rhoLc = rho_LOC[k];
		u1 = u_1_LOC[k];
		u2 = u_2_LOC[k];
		gamAux = weight * ( - 1.5*(u1*u1 + u2*u2 ) + 3.0*(u1+u2) + 4.5*(u1+u2)*(u1+u2) );
		gamma = weight + gamAux;
		gEQ = 3.0*prssr_LOC[k]*weight + rhoLc*gamAux;
		tau = tauVp + (rhoLc - rhoV)*relTauRhoRatio;
		degRate = 1.0/(2.0*tau + 1.0);
		dirDrv = 0.5*( rho_LOC[nnP[k]] - rho_LOC[nnM[k]]);
		
		dirDrvB = 0.5*( B_LOC[nnP[k]] - B_LOC[nnM[k]] );
		dirDrvC_x = 0.5*( dx_LOC[nnP[k]] - dx_LOC[nnM[k]] );
		dirDrvC_y = 0.5*( dy_LOC[nnP[k]] - dy_LOC[nnM[k]] );
		eD = 1.5*kappa*(dirDrvB - dirDrv*lapRho_LOC[k] - dx_LOC[k]*dirDrvC_x - dy_LOC[k]*dirDrvC_y);

		EU = dx_LOC[k]*u1+dy_LOC[k]*u2;
		G = Gconst*rhoLc*(	1.0+2.0*u2+3.0*u1+gravConst	);

		g_5[k] = g_5[k] + degRate*( gEQ - g_5[k] +2.0*tau*( (eD - Du_LOC[k])*gamma +0.5*( dirDrv - EU )*gamAux + G )			);
	}
}

void postStr_G6(){
	arrayCopy(distr_Loc, g_6, lenAr);
	nnP = nn_6;
	nnM = nn_8;

	fltp weight = w3;
	
	fltp gEQ,gamma,gamAux,u1,u2,degRate,EU,dirDrv,dirDrvB,dirDrvC_x,dirDrvC_y,eD,G,tau,rhoLc;
	
	for (int k=0; k<lenAr; k++){

		rhoLc = rho_LOC[k];
		u1 = u_1_LOC[k];
		u2 = u_2_LOC[k];
		gamAux = weight * ( - 1.5*(u1*u1 + u2*u2 ) +3.0*(-u1+u2) + 4.5*(-u1+u2)*(-u1+u2) );
		gamma = weight + gamAux;
		gEQ = 3.0*prssr_LOC[k]*weight + rhoLc*gamAux;
		tau = tauVp + (rhoLc - rhoV)*relTauRhoRatio;
		degRate = 1.0/(2.0*tau + 1.0);
		dirDrv = 0.5*( rho_LOC[nnP[k]] - rho_LOC[nnM[k]]);
		
		dirDrvB = 0.5*( B_LOC[nnP[k]] - B_LOC[nnM[k]] );
		dirDrvC_x = 0.5*( dx_LOC[nnP[k]] - dx_LOC[nnM[k]] );
		dirDrvC_y = 0.5*( dy_LOC[nnP[k]] - dy_LOC[nnM[k]] );
		eD = 1.5*kappa*(dirDrvB - dirDrv*lapRho_LOC[k] - dx_LOC[k]*dirDrvC_x - dy_LOC[k]*dirDrvC_y);

		EU = dx_LOC[k]*u1+dy_LOC[k]*u2;
		G = Gconst*rhoLc*( 1.0+2.0*u2 - 3.0*u1 + gravConst );

		g_6[k] = g_6[k] + degRate*( gEQ - g_6[k] +2.0*tau*( (eD - Du_LOC[k])*gamma +0.5*( dirDrv - EU )*gamAux + G )			);
	}
}

void postStr_G7(){
	arrayCopy(distr_Loc, g_7, lenAr);
	nnP = nn_7;
	nnM = nn_5;

	fltp weight = w3;
	
	fltp gEQ,gamma,gamAux,u1,u2,degRate,EU,dirDrv,dirDrvB,dirDrvC_x,dirDrvC_y,eD,G,tau,rhoLc;

	for (int k=0; k<lenAr; k++){

		rhoLc = rho_LOC[k];
		u1 = u_1_LOC[k];
		u2 = u_2_LOC[k];
		gamAux = weight * ( - 1.5*(u1*u1 + u2*u2 ) - 3.0*(u1+u2) + 4.5*(u1+u2)*(u1+u2) );
		gamma = weight + gamAux;
		gEQ = 3.0*prssr_LOC[k]*weight + rhoLc*gamAux;
		tau = tauVp + (rhoLc - rhoV)*relTauRhoRatio;
		degRate = 1.0/(2.0*tau + 1.0);
		dirDrv = 0.5*( rho_LOC[nnP[k]] - rho_LOC[nnM[k]]);
		
		dirDrvB = 0.5*( B_LOC[nnP[k]] - B_LOC[nnM[k]] );
		dirDrvC_x = 0.5*( dx_LOC[nnP[k]] - dx_LOC[nnM[k]] );
		dirDrvC_y = 0.5*( dy_LOC[nnP[k]] - dy_LOC[nnM[k]] );
		eD = 1.5*kappa*(dirDrvB - dirDrv*lapRho_LOC[k] - dx_LOC[k]*dirDrvC_x - dy_LOC[k]*dirDrvC_y);

		EU = dx_LOC[k]*u1+dy_LOC[k]*u2;
		G = -Gconst*rhoLc*( 1.0 + 4.0*u2 + 3.0*u1 - gravConst );

		g_7[k] = g_7[k] + degRate*( gEQ - g_7[k] +2.0*tau*( (eD - Du_LOC[k])*gamma +0.5*( dirDrv - EU )*gamAux + G )			);
	}
}

void postStr_G8(){
	arrayCopy(distr_Loc, g_8, lenAr);
	nnP = nn_8;
	nnM = nn_6;

	fltp weight = w3;
	
	fltp gEQ,gamma,gamAux,u1,u2,degRate,EU,dirDrv,dirDrvB,dirDrvC_x,dirDrvC_y,eD,G,tau,rhoLc;

	for (int k=0; k<lenAr; k++){

		rhoLc = rho_LOC[k];
		u1 = u_1_LOC[k];
		u2 = u_2_LOC[k];
		gamAux = weight * ( - 1.5*(u1*u1 + u2*u2 ) + 3.0*(u1-u2) + 4.5*(u1-u2)*(u1-u2) );
		gamma = weight + gamAux;
		gEQ = 3.0*prssr_LOC[k]*weight + rhoLc*gamAux;
		tau = tauVp + (rhoLc - rhoV)*relTauRhoRatio;
		degRate = 1.0/(2.0*tau + 1.0);
		dirDrv = 0.5*( rho_LOC[nnP[k]] - rho_LOC[nnM[k]]);
		
		dirDrvB = 0.5*( B_LOC[nnP[k]] - B_LOC[nnM[k]] );
		dirDrvC_x = 0.5*( dx_LOC[nnP[k]] - dx_LOC[nnM[k]] );
		dirDrvC_y = 0.5*( dy_LOC[nnP[k]] - dy_LOC[nnM[k]] );
		eD = 1.5*kappa*(dirDrvB - dirDrv*lapRho_LOC[k] - dx_LOC[k]*dirDrvC_x - dy_LOC[k]*dirDrvC_y);

		EU = dx_LOC[k]*u1+dy_LOC[k]*u2;
		G = -Gconst*rhoLc*( 1.0 + 4.0*u2 - 3.0*u1 - gravConst );

		g_8[k] = g_8[k] + degRate*( gEQ - g_8[k] +2.0*tau*( (eD - Du_LOC[k])*gamma +0.5*( dirDrv - EU )*gamAux + G )			);
	}
}


// -----------order parameter field:
void postStr_F0(){
	arrayCopy(distr_Loc, f_0, lenAr);

	fltp weight,rhoLc,u1,u2,degRate,gamma,H,tau;
	weight = w1;
	
	for( int k=0; k<lenAr; k++){
		rhoLc = rho_LOC[k];
		u1 = u_1_LOC[k];
		u2 = u_2_LOC[k];
		H = -dx_LOC[k]*u1 -dy_LOC[k]*u2 +3.0*rhoLc*( +dxPsi_LOC[k]*u1 +dyPsi_LOC[k]*u2 );
		gamma = weight * ( 1.0 - 1.5*(u1*u1 + u2*u2 ) );

		tau = tauVp + (rhoLc - rhoV)*relTauRhoRatio;
		degRate = 1.0/(2.0*tau +1.0);

		f_0[k] = distr_Loc[k] +(rhoLc*gamma - distr_Loc[k])*degRate	+ tau*degRate*H*gamma ;
	}
}

void postStr_F1(){
	arrayCopy(distr_Loc, f_1, lenAr);
	nnP = nn_1;
	nnM = nn_3;

	fltp weight,rhoLc,u1,u2,degRate,gamma,dirDrv,dirDrvPsi,H,tau;
	weight = w2;
	
	for( int k=0; k<lenAr; k++){
		rhoLc = rho_LOC[k];
		u1 = u_1_LOC[k];
		u2 = u_2_LOC[k];
		dirDrv = 0.5*( rho_LOC[nnP[k]] - rho_LOC[nnM[k]] );
		dirDrvPsi = 0.5*( psi_LOC[nnP[k]] - psi_LOC[nnM[k]] );

		H = dirDrv - dx_LOC[k]*u1 - dy_LOC[k]*u2 -3.0*rhoLc*( dirDrvPsi - dxPsi_LOC[k]*u1 - dyPsi_LOC[k]*u2 );
		gamma = weight * ( 1.0 - 1.5*(u1*u1 + u2*u2 ) + 3.0*u1 + 4.5*u1*u1 );

		tau = tauVp + (rhoLc - rhoV)*relTauRhoRatio;
		degRate = 1.0/(2.0*tau +1.0);

		f_1[k] = distr_Loc[k] +(rhoLc*gamma - distr_Loc[k])*degRate + tau*degRate*H*gamma 	;
	}
}

void postStr_F2(){
	arrayCopy(distr_Loc, f_2, lenAr);
	nnP = nn_2;
	nnM = nn_4;

	fltp weight,rhoLc,u1,u2,degRate,gamma,dirDrv,dirDrvPsi,H,tau;
	weight = w2;
	
	for( int k=0; k<lenAr; k++){
		rhoLc = rho_LOC[k];
		u1 = u_1_LOC[k];
		u2 = u_2_LOC[k];
		dirDrv = 0.5*( rho_LOC[nnP[k]] - rho_LOC[nnM[k]] );
		dirDrvPsi = 0.5*( psi_LOC[nnP[k]] - psi_LOC[nnM[k]] );

		H = dirDrv - dx_LOC[k]*u1 - dy_LOC[k]*u2 -3.0*rhoLc*( dirDrvPsi - dxPsi_LOC[k]*u1 - dyPsi_LOC[k]*u2 );
		gamma = weight * ( 1.0 - 1.5*(u1*u1 + u2*u2 )  + 3.0*u2 + 4.5*u2*u2 );

		tau = tauVp + (rhoLc - rhoV)*relTauRhoRatio;
		degRate = 1.0/(2.0*tau +1.0);

		f_2[k] = distr_Loc[k] +(rhoLc*gamma - distr_Loc[k])*degRate + tau*degRate*H*gamma 	;
	}
}

void postStr_F3(){
	arrayCopy(distr_Loc, f_3, lenAr);
	nnP = nn_3;
	nnM = nn_1;

	fltp weight,rhoLc,u1,u2,degRate,gamma,dirDrv,dirDrvPsi,H,tau;
	weight = w2;
	
	for( int k=0; k<lenAr; k++){
		rhoLc = rho_LOC[k];
		u1 = u_1_LOC[k];
		u2 = u_2_LOC[k];
		dirDrv = 0.5*( rho_LOC[nnP[k]] - rho_LOC[nnM[k]] );
		dirDrvPsi = 0.5*( psi_LOC[nnP[k]] - psi_LOC[nnM[k]] );

		H = dirDrv - dx_LOC[k]*u1 - dy_LOC[k]*u2 -3.0*rhoLc*( dirDrvPsi - dxPsi_LOC[k]*u1 - dyPsi_LOC[k]*u2 );
		gamma = weight * ( 1.0 - 1.5*(u1*u1 + u2*u2 ) -3.0*u1 + 4.5*u1*u1 );

		tau = tauVp + (rhoLc - rhoV)*relTauRhoRatio;
		degRate = 1.0/(2.0*tau +1.0);

		f_3[k] = distr_Loc[k] +(rhoLc*gamma - distr_Loc[k])*degRate + tau*degRate*H*gamma 	;
	}
}

void postStr_F4(){
	arrayCopy(distr_Loc, f_4, lenAr);
	nnP = nn_4;
	nnM = nn_2;

	fltp weight,rhoLc,u1,u2,degRate,gamma,dirDrv,dirDrvPsi,H,tau;
	weight = w2;
	
	for( int k=0; k<lenAr; k++){
		rhoLc = rho_LOC[k];
		u1 = u_1_LOC[k];
		u2 = u_2_LOC[k];
		dirDrv = 0.5*( rho_LOC[nnP[k]] - rho_LOC[nnM[k]] );
		dirDrvPsi = 0.5*( psi_LOC[nnP[k]] - psi_LOC[nnM[k]] );

		H = dirDrv - dx_LOC[k]*u1 - dy_LOC[k]*u2 -3.0*rhoLc*( dirDrvPsi - dxPsi_LOC[k]*u1 - dyPsi_LOC[k]*u2 );
		gamma = weight * ( 1.0 - 1.5*(u1*u1 + u2*u2 ) -3.0*u2 + 4.5*u2*u2 );

		tau = tauVp + (rhoLc - rhoV)*relTauRhoRatio;
		degRate = 1.0/(2.0*tau +1.0);

		f_4[k] = distr_Loc[k] +(rhoLc*gamma - distr_Loc[k])*degRate + tau*degRate*H*gamma 	;
	}
}

void postStr_F5(){
	arrayCopy(distr_Loc, f_5, lenAr);
	nnP = nn_5;
	nnM = nn_7;

	fltp weight,rhoLc,u1,u2,degRate,gamma,dirDrv,dirDrvPsi,H,tau;
	weight = w3;
	
	for( int k=0; k<lenAr; k++){
		rhoLc = rho_LOC[k];
		u1 = u_1_LOC[k];
		u2 = u_2_LOC[k];
		dirDrv = 0.5*( rho_LOC[nnP[k]] - rho_LOC[nnM[k]] );
		dirDrvPsi = 0.5*( psi_LOC[nnP[k]] - psi_LOC[nnM[k]] );

		H = dirDrv - dx_LOC[k]*u1 - dy_LOC[k]*u2 -3.0*rhoLc*( dirDrvPsi - dxPsi_LOC[k]*u1 - dyPsi_LOC[k]*u2 );
		gamma = weight * ( 1.0 - 1.5*(u1*u1 + u2*u2 ) + 3.0*(u1+u2) + 4.5*(u1+u2)*(u1+u2) );

		tau = tauVp + (rhoLc - rhoV)*relTauRhoRatio;
		degRate = 1.0/(2.0*tau +1.0);

		f_5[k] = distr_Loc[k] +(rhoLc*gamma - distr_Loc[k])*degRate + tau*degRate*H*gamma ;
	}
}

void postStr_F6(){
	arrayCopy(distr_Loc, f_6, lenAr);
	nnP = nn_6;
	nnM = nn_8;

	fltp weight,rhoLc,u1,u2,degRate,gamma,dirDrv,dirDrvPsi,H,tau;
	weight = w3;
	
	for( int k=0; k<lenAr; k++){
		rhoLc = rho_LOC[k];
		u1 = u_1_LOC[k];
		u2 = u_2_LOC[k];
		dirDrv = 0.5*( rho_LOC[nnP[k]] - rho_LOC[nnM[k]] );
		dirDrvPsi = 0.5*( psi_LOC[nnP[k]] - psi_LOC[nnM[k]] );

		H = dirDrv - dx_LOC[k]*u1 - dy_LOC[k]*u2 -3.0*rhoLc*( dirDrvPsi - dxPsi_LOC[k]*u1 - dyPsi_LOC[k]*u2 );
		gamma = weight * ( 1.0 - 1.5*(u1*u1 + u2*u2 ) +3.0*(-u1+u2) + 4.5*(-u1+u2)*(-u1+u2) );

		tau = tauVp + (rhoLc - rhoV)*relTauRhoRatio;
		degRate = 1.0/(2.0*tau +1.0);

		f_6[k] = distr_Loc[k] +(rhoLc*gamma - distr_Loc[k])*degRate + tau*degRate*H*gamma ;
	}
}

void postStr_F7(){
	arrayCopy(distr_Loc, f_7, lenAr);
	nnP = nn_7;
	nnM = nn_5;

	fltp weight,rhoLc,u1,u2,degRate,gamma,dirDrv,dirDrvPsi,H,tau;
	weight = w3;
	
	for( int k=0; k<lenAr; k++){
		rhoLc = rho_LOC[k];
		u1 = u_1_LOC[k];
		u2 = u_2_LOC[k];
		dirDrv = 0.5*( rho_LOC[nnP[k]] - rho_LOC[nnM[k]] );
		dirDrvPsi = 0.5*( psi_LOC[nnP[k]] - psi_LOC[nnM[k]] );

		H = dirDrv - dx_LOC[k]*u1 - dy_LOC[k]*u2 -3.0*rhoLc*( dirDrvPsi - dxPsi_LOC[k]*u1 - dyPsi_LOC[k]*u2 );
		gamma = weight * ( 1.0 - 1.5*(u1*u1 + u2*u2 ) - 3.0*(u1+u2) + 4.5*(u1+u2)*(u1+u2) );

		tau = tauVp + (rhoLc - rhoV)*relTauRhoRatio;
		degRate = 1.0/(2.0*tau +1.0);

		f_7[k] = distr_Loc[k] +(rhoLc*gamma - distr_Loc[k])*degRate + tau*degRate*H*gamma 	;
	}
}

void postStr_F8(){
	arrayCopy(distr_Loc, f_8, lenAr);
	nnP = nn_8;
	nnM = nn_6;

	fltp weight,rhoLc,u1,u2,degRate,gamma,dirDrv,dirDrvPsi,H,tau;
	weight = w3;
	
	for( int k=0; k<lenAr; k++){
		rhoLc = rho_LOC[k];
		u1 = u_1_LOC[k];
		u2 = u_2_LOC[k];
		dirDrv = 0.5*( rho_LOC[nnP[k]] - rho_LOC[nnM[k]] );
		dirDrvPsi = 0.5*( psi_LOC[nnP[k]] - psi_LOC[nnM[k]] );

		H = dirDrv - dx_LOC[k]*u1 - dy_LOC[k]*u2 -3.0*rhoLc*( dirDrvPsi - dxPsi_LOC[k]*u1 - dyPsi_LOC[k]*u2 );
		gamma = weight * ( 1.0 - 1.5*(u1*u1 + u2*u2 ) + 3.0*(u1-u2) + 4.5*(u1-u2)*(u1-u2) );

		tau = tauVp + (rhoLc - rhoV)*relTauRhoRatio;
		degRate = 1.0/(2.0*tau +1.0);

		f_8[k] = distr_Loc[k] +(rhoLc*gamma - distr_Loc[k])*degRate + tau*degRate*H*gamma 	;
	}
}

