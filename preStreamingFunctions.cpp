// preStreamingFunctions.cpp



//-------------------------------- pre-streaming functions -------------------------------------

void preStr_G0(){
			
	arrayCopy(distr_Loc, g_0, lenAr);
	
	fltp weight = w1;
	
	fltp g,gEQ,gamma,gamAux,u1,u2,degRate,rhoLc,EU,G;

	for(int k=0; k<lenAr; k++){
		g = distr_Loc[k];
		rhoLc = rho_LOC[k];
		u1 = u_1_LOC[k];
		u2 = u_2_LOC[k];

		EU = dx_LOC[k]*u1 +dy_LOC[k]*u2;

		G = -Gconst*16.0*rhoLc*( u2-0.5*gravConst );
		degRate = 0.5/(tauVp + (rhoLc - rhoV)*relTauRhoRatio);

		//calculating the equilibirum function:
		gamAux = weight * ( - 1.5*(u1*u1 + u2*u2 ) );
		gamma = weight + gamAux;
		gEQ = 3.0*prssr_LOC[k]*weight + rhoLc*gamAux;

		g_0[k] = g +(gEQ - g)*degRate - Du_LOC[k]*gamma - 0.5*EU*gamAux + G;
	}
}

void preStr_G1(){
			
	arrayCopy(distr_Loc, g_1, lenAr);
	nnP = nn_1;
	nnM = nn_3;
	
	fltp weight = w2;	
	
	fltp g,gEQ,gamma,gamAux,u1,u2,degRate,rhoLc,eD,EU,dirDrv,dirDrvB,dirDrvC_x,dirDrvC_y,G;
	
	for(int k=0; k<lenAr; k++){
		g = distr_Loc[k];
		rhoLc = rho_LOC[k];
		u1 = u_1_LOC[k];
		u2 = u_2_LOC[k];
		
		EU = dx_LOC[k]*u1 +dy_LOC[k]*u2;
		dirDrv = 0.5*( -rho_LOC[nnP[nnP[k]]] +4.0*rho_LOC[nnP[k]] -3.0*rhoLc );
		if( ( rho_LOC[nnP[nnP[k]]] - rho_LOC[k] )*dirDrv < 0.0  ){
			dirDrv = 0.5*( rho_LOC[nnP[k]] - rho_LOC[nnM[k]] );
		}
		// dirDrvB = 0.5*( -B_LOC[nnP[nnP[k]]] +4.0*B_LOC[nnP[k]] -3.0*rhoLc );
		// if( ( B_LOC[nnP[nnP[k]]] - B_LOC[k] )*dirDrvB < 0.0  ){
		// 	dirDrvB = 0.5*( B_LOC[nnP[k]] - B_LOC[nnM[k]] );
		// }
		// dirDrvC_x = 0.5*( -dx_LOC[nnP[nnP[k]]] +4.0*dx_LOC[nnP[k]] -3.0*rhoLc );
		// if( ( dx_LOC[nnP[nnP[k]]] - dx_LOC[k] )*dirDrvC_x < 0.0  ){
		// 	dirDrvC_x = 0.5*( dx_LOC[nnP[k]] - dx_LOC[nnM[k]] );
		// }
		// dirDrvC_y = 0.5*( -dy_LOC[nnP[nnP[k]]] +4.0*dy_LOC[nnP[k]] -3.0*rhoLc );
		// if( ( dy_LOC[nnP[nnP[k]]] - dy_LOC[k] )*dirDrvC_y < 0.0  ){
		// 	dirDrvC_y = 0.5*( dy_LOC[nnP[k]] - dy_LOC[nnM[k]] );
		// }

		dirDrvB = 0.5*( B_LOC[nnP[k]] - B_LOC[nnM[k]] );
		dirDrvC_x = 0.5*( dx_LOC[nnP[k]] - dx_LOC[nnM[k]] );
		dirDrvC_y = 0.5*( dy_LOC[nnP[k]] - dy_LOC[nnM[k]] );
		eD = 1.5*kappa*(dirDrvB - dirDrv*lapRho_LOC[k] - dx_LOC[k]*dirDrvC_x - dy_LOC[k]*dirDrvC_y);
		// eD = 0.0;
		G = Gconst*4.0*rhoLc*( 3.0*u1 - u2 - 0.5*gravConst );
		degRate = 0.5/(tauVp + (rhoLc - rhoV)*relTauRhoRatio);
		//calculating the equilibirum function:
		gamAux = weight * ( - 1.5*(u1*u1 + u2*u2 ) + 3.0*u1 + 4.5*u1*u1 );
		gamma = weight + gamAux;
		gEQ = 3.0*prssr_LOC[k]*weight + rhoLc*gamAux;
	
		g_1[nnP[k]] = g+(gEQ - g)*degRate + ( eD - Du_LOC[k] )*gamma + 0.5*( dirDrv - EU )*gamAux + G;
	}
}
	
void preStr_G2(){

	arrayCopy(distr_Loc, g_2, lenAr);
	nnP = nn_2;
	nnM = nn_4;
	
	fltp weight = w2;
	
	fltp g,gEQ,gamma,gamAux,u1,u2,degRate,rhoLc,eD,EU,dirDrv,dirDrvB,dirDrvC_x,dirDrvC_y,G;

	for(int k=0; k<lenAr; k++){
		g = distr_Loc[k];
		rhoLc = rho_LOC[k];
		u1 = u_1_LOC[k];
		u2 = u_2_LOC[k];
		
		EU = dx_LOC[k]*u1+dy_LOC[k]*u2;
		dirDrv = 0.5*( -rho_LOC[nnP[nnP[k]]] +4.0*rho_LOC[nnP[k]] -3.0*rhoLc );
		if( ( rho_LOC[nnP[nnP[k]]] - rho_LOC[k] )*dirDrv < 0.0  ){
			dirDrv = 0.5*( rho_LOC[nnP[k]] - rho_LOC[nnM[k]] );
		}
		// dirDrvB = 0.5*( -B_LOC[nnP[nnP[k]]] +4.0*B_LOC[nnP[k]] -3.0*rhoLc );
		// if( ( B_LOC[nnP[nnP[k]]] - B_LOC[k] )*dirDrvB < 0.0  ){
		// 	dirDrvB = 0.5*( B_LOC[nnP[k]] - B_LOC[nnM[k]] );
		// }
		// dirDrvC_x = 0.5*( -dx_LOC[nnP[nnP[k]]] +4.0*dx_LOC[nnP[k]] -3.0*rhoLc );
		// if( ( dx_LOC[nnP[nnP[k]]] - dx_LOC[k] )*dirDrvC_x < 0.0  ){
		// 	dirDrvC_x = 0.5*( dx_LOC[nnP[k]] - dx_LOC[nnM[k]] );
		// }
		// dirDrvC_y = 0.5*( -dy_LOC[nnP[nnP[k]]] +4.0*dy_LOC[nnP[k]] -3.0*rhoLc );
		// if( ( dy_LOC[nnP[nnP[k]]] - dy_LOC[k] )*dirDrvC_y < 0.0  ){
		// 	dirDrvC_y = 0.5*( dy_LOC[nnP[k]] - dy_LOC[nnM[k]] );
		// }
		dirDrvB = 0.5*( B_LOC[nnP[k]] - B_LOC[nnM[k]] );
		dirDrvC_x = 0.5*( dx_LOC[nnP[k]] - dx_LOC[nnM[k]] );
		dirDrvC_y = 0.5*( dy_LOC[nnP[k]] - dy_LOC[nnM[k]] );
		eD = 1.5*kappa*(dirDrvB - dirDrv*lapRho_LOC[k] - dx_LOC[k]*dirDrvC_x - dy_LOC[k]*dirDrvC_y);

		G = Gconst*4.0*rhoLc*( 1.0+2.0*u2 + gravConst );
		degRate = 0.5/(tauVp + (rhoLc - rhoV)*relTauRhoRatio);
		//calculating the equilibirum function:
		gamAux = weight * ( - 1.5*(u1*u1 + u2*u2 )  + 3.0*u2 + 4.5*u2*u2 );
		gamma = weight + gamAux;
		gEQ = 3.0*prssr_LOC[k]*weight + rhoLc*gamAux;

		g_2[nnP[k]] = g+(gEQ - g)*degRate + ( eD - Du_LOC[k] )*gamma  + 0.5*( dirDrv - EU )*gamAux + G;
	}
}

void preStr_G3(){
	
	arrayCopy(distr_Loc, g_3, lenAr);
	nnP = nn_3;
	nnM = nn_1;
	
	fltp weight = w2;
	
	fltp g,gEQ,gamma,gamAux,u1,u2,degRate,rhoLc,eD,EU,dirDrv,dirDrvB,dirDrvC_x,dirDrvC_y,G;

	for(int k=0; k<lenAr; k++){
		g = distr_Loc[k];
		rhoLc = rho_LOC[k];
		u1 = u_1_LOC[k];
		u2 = u_2_LOC[k];
		
		EU = dx_LOC[k]*u1+dy_LOC[k]*u2;
		dirDrv = 0.5*( -rho_LOC[nnP[nnP[k]]] +4.0*rho_LOC[nnP[k]] -3.0*rhoLc );
		if( ( rho_LOC[nnP[nnP[k]]] - rho_LOC[k] )*dirDrv < 0.0  ){
			dirDrv = 0.5*( rho_LOC[nnP[k]] - rho_LOC[nnM[k]] );
		}
		// dirDrvB = 0.5*( -B_LOC[nnP[nnP[k]]] +4.0*B_LOC[nnP[k]] -3.0*rhoLc );
		// if( ( B_LOC[nnP[nnP[k]]] - B_LOC[k] )*dirDrvB < 0.0  ){
		// 	dirDrvB = 0.5*( B_LOC[nnP[k]] - B_LOC[nnM[k]] );
		// }
		// dirDrvC_x = 0.5*( -dx_LOC[nnP[nnP[k]]] +4.0*dx_LOC[nnP[k]] -3.0*rhoLc );
		// if( ( dx_LOC[nnP[nnP[k]]] - dx_LOC[k] )*dirDrvC_x < 0.0  ){
		// 	dirDrvC_x = 0.5*( dx_LOC[nnP[k]] - dx_LOC[nnM[k]] );
		// }
		// dirDrvC_y = 0.5*( -dy_LOC[nnP[nnP[k]]] +4.0*dy_LOC[nnP[k]] -3.0*rhoLc );
		// if( ( dy_LOC[nnP[nnP[k]]] - dy_LOC[k] )*dirDrvC_y < 0.0  ){
		// 	dirDrvC_y = 0.5*( dy_LOC[nnP[k]] - dy_LOC[nnM[k]] );
		// }
		
		dirDrvB = 0.5*( B_LOC[nnP[k]] - B_LOC[nnM[k]] );
		dirDrvC_x = 0.5*( dx_LOC[nnP[k]] - dx_LOC[nnM[k]] );
		dirDrvC_y = 0.5*( dy_LOC[nnP[k]] - dy_LOC[nnM[k]] );
		eD = 1.5*kappa*(dirDrvB - dirDrv*lapRho_LOC[k] - dx_LOC[k]*dirDrvC_x - dy_LOC[k]*dirDrvC_y);

		G = -Gconst*4.0*rhoLc*( 3.0*u1 + u2 + 0.5*gravConst );
		degRate = 0.5/(tauVp + (rhoLc - rhoV)*relTauRhoRatio);
		//calculating the equilibirum function:
		gamAux = weight * ( - 1.5*(u1*u1 + u2*u2 ) -3.0*u1 + 4.5*u1*u1 );
		gamma = weight + gamAux;
		gEQ = 3.0*prssr_LOC[k]*weight + rhoLc*gamAux;

		g_3[nnP[k]] = g+(gEQ - g)*degRate + ( eD - Du_LOC[k] )*gamma + 0.5*( dirDrv - EU )*gamAux + G;
	}
}

void preStr_G4(){
	
	arrayCopy(distr_Loc, g_4, lenAr);
	nnP = nn_4;
	nnM = nn_2;
	
	fltp weight = w2;
	
	fltp g,gEQ,gamma,gamAux,u1,u2,degRate,rhoLc,eD,EU,dirDrv,dirDrvB,dirDrvC_x,dirDrvC_y,G;

	for(int k=0; k<lenAr; k++){
		g = distr_Loc[k];
		rhoLc = rho_LOC[k];
		u1 = u_1_LOC[k];
		u2 = u_2_LOC[k];
		
		EU = dx_LOC[k]*u1+dy_LOC[k]*u2;
		dirDrv = 0.5*( -rho_LOC[nnP[nnP[k]]] +4.0*rho_LOC[nnP[k]] -3.0*rhoLc );
		if( ( rho_LOC[nnP[nnP[k]]] - rho_LOC[k] )*dirDrv < 0.0  ){
			dirDrv = 0.5*( rho_LOC[nnP[k]] - rho_LOC[nnM[k]] );
		}
		// dirDrvB = 0.5*( -B_LOC[nnP[nnP[k]]] +4.0*B_LOC[nnP[k]] -3.0*rhoLc );
		// if( ( B_LOC[nnP[nnP[k]]] - B_LOC[k] )*dirDrvB < 0.0  ){
		// 	dirDrvB = 0.5*( B_LOC[nnP[k]] - B_LOC[nnM[k]] );
		// }
		// dirDrvC_x = 0.5*( -dx_LOC[nnP[nnP[k]]] +4.0*dx_LOC[nnP[k]] -3.0*rhoLc );
		// if( ( dx_LOC[nnP[nnP[k]]] - dx_LOC[k] )*dirDrvC_x < 0.0  ){
		// 	dirDrvC_x = 0.5*( dx_LOC[nnP[k]] - dx_LOC[nnM[k]] );
		// }
		// dirDrvC_y = 0.5*( -dy_LOC[nnP[nnP[k]]] +4.0*dy_LOC[nnP[k]] -3.0*rhoLc );
		// if( ( dy_LOC[nnP[nnP[k]]] - dy_LOC[k] )*dirDrvC_y < 0.0  ){
		// 	dirDrvC_y = 0.5*( dy_LOC[nnP[k]] - dy_LOC[nnM[k]] );
		// }
		
		dirDrvB = 0.5*( B_LOC[nnP[k]] - B_LOC[nnM[k]] );
		dirDrvC_x = 0.5*( dx_LOC[nnP[k]] - dx_LOC[nnM[k]] );
		dirDrvC_y = 0.5*( dy_LOC[nnP[k]] - dy_LOC[nnM[k]] );
		eD = 1.5*kappa*(dirDrvB - dirDrv*lapRho_LOC[k] - dx_LOC[k]*dirDrvC_x - dy_LOC[k]*dirDrvC_y);

		G = -Gconst*4.0*rhoLc*( 1.0+4.0*u2-gravConst );
		degRate = 0.5/(tauVp + (rhoLc - rhoV)*relTauRhoRatio);
		//calculating the equilibirum function:
		gamAux = weight * ( - 1.5*(u1*u1 + u2*u2 ) -3.0*u2 + 4.5*u2*u2 );
		gamma = weight + gamAux;
		gEQ = 3.0*prssr_LOC[k]*weight + rhoLc*gamAux;

		g_4[nnP[k]] = g+(gEQ - g)*degRate + ( eD - Du_LOC[k] )*gamma  + 0.5*( dirDrv - EU )*gamAux + G;
	}
}

void preStr_G5(){
	
	arrayCopy(distr_Loc, g_5, lenAr);
	nnP = nn_5;
	nnM = nn_7;
	
	
	fltp weight = w3;
	
	fltp g,gEQ,gamma,gamAux,u1,u2,degRate,rhoLc,eD,EU,dirDrv,dirDrvB,dirDrvC_x,dirDrvC_y,G;

	for(int k=0; k<lenAr; k++){
		g = distr_Loc[k];
		rhoLc = rho_LOC[k];
		u1 = u_1_LOC[k];
		u2 = u_2_LOC[k];
		
		EU = dx_LOC[k]*u1+dy_LOC[k]*u2;
		dirDrv = 0.5*( -rho_LOC[nnP[nnP[k]]] +4.0*rho_LOC[nnP[k]] -3.0*rhoLc );
		if( ( rho_LOC[nnP[nnP[k]]] - rho_LOC[k] )*dirDrv < 0.0  ){
			dirDrv = 0.5*( rho_LOC[nnP[k]] - rho_LOC[nnM[k]] );
		}
		// dirDrvB = 0.5*( -B_LOC[nnP[nnP[k]]] +4.0*B_LOC[nnP[k]] -3.0*rhoLc );
		// if( ( B_LOC[nnP[nnP[k]]] - B_LOC[k] )*dirDrvB < 0.0  ){
		// 	dirDrvB = 0.5*( B_LOC[nnP[k]] - B_LOC[nnM[k]] );
		// }
		// dirDrvC_x = 0.5*( -dx_LOC[nnP[nnP[k]]] +4.0*dx_LOC[nnP[k]] -3.0*rhoLc );
		// if( ( dx_LOC[nnP[nnP[k]]] - dx_LOC[k] )*dirDrvC_x < 0.0  ){
		// 	dirDrvC_x = 0.5*( dx_LOC[nnP[k]] - dx_LOC[nnM[k]] );
		// }
		// dirDrvC_y = 0.5*( -dy_LOC[nnP[nnP[k]]] +4.0*dy_LOC[nnP[k]] -3.0*rhoLc );
		// if( ( dy_LOC[nnP[nnP[k]]] - dy_LOC[k] )*dirDrvC_y < 0.0  ){
		// 	dirDrvC_y = 0.5*( dy_LOC[nnP[k]] - dy_LOC[nnM[k]] );
		// }

		dirDrvB = 0.5*( B_LOC[nnP[k]] - B_LOC[nnM[k]] );
		dirDrvC_x = 0.5*( dx_LOC[nnP[k]] - dx_LOC[nnM[k]] );
		dirDrvC_y = 0.5*( dy_LOC[nnP[k]] - dy_LOC[nnM[k]] );
		eD = 1.5*kappa*(dirDrvB - dirDrv*lapRho_LOC[k] - dx_LOC[k]*dirDrvC_x - dy_LOC[k]*dirDrvC_y);

		G = Gconst*rhoLc*(	1.0+2.0*u2+3.0*u1+gravConst	);
		degRate = 0.5/(tauVp + (rhoLc - rhoV)*relTauRhoRatio);

		gamAux = weight * ( - 1.5*(u1*u1 + u2*u2 ) + 3.0*(u1+u2) + 4.5*(u1+u2)*(u1+u2) );
		gamma = weight + gamAux;
		gEQ = 3.0*prssr_LOC[k]*weight + rhoLc*gamAux;

		g_5[nnP[k]] = g+(gEQ - g)*degRate + ( eD - Du_LOC[k] )*gamma  + 0.5*( dirDrv - EU )*gamAux + G;
	}
}	

void preStr_G6(){

	arrayCopy(distr_Loc, g_6, lenAr);
	nnP = nn_6;
	nnM = nn_8;
	
	fltp weight = w3;
	
	fltp g,gEQ,gamma,gamAux,u1,u2,degRate,rhoLc,eD,EU,dirDrv,dirDrvB,dirDrvC_x,dirDrvC_y,G;

	for(int k=0; k<lenAr; k++){
		g = distr_Loc[k];
		rhoLc = rho_LOC[k];
		u1 = u_1_LOC[k];
		u2 = u_2_LOC[k];
		
		EU = dx_LOC[k]*u1+dy_LOC[k]*u2;
		dirDrv = 0.5*( -rho_LOC[nnP[nnP[k]]] +4.0*rho_LOC[nnP[k]] -3.0*rhoLc );
		if( ( rho_LOC[nnP[nnP[k]]] - rho_LOC[k] )*dirDrv < 0.0  ){
			dirDrv = 0.5*( rho_LOC[nnP[k]] - rho_LOC[nnM[k]] );
		}
		// dirDrvB = 0.5*( -B_LOC[nnP[nnP[k]]] +4.0*B_LOC[nnP[k]] -3.0*rhoLc );
		// if( ( B_LOC[nnP[nnP[k]]] - B_LOC[k] )*dirDrvB < 0.0  ){
		// 	dirDrvB = 0.5*( B_LOC[nnP[k]] - B_LOC[nnM[k]] );
		// }
		// dirDrvC_x = 0.5*( -dx_LOC[nnP[nnP[k]]] +4.0*dx_LOC[nnP[k]] -3.0*rhoLc );
		// if( ( dx_LOC[nnP[nnP[k]]] - dx_LOC[k] )*dirDrvC_x < 0.0  ){
		// 	dirDrvC_x = 0.5*( dx_LOC[nnP[k]] - dx_LOC[nnM[k]] );
		// }
		// dirDrvC_y = 0.5*( -dy_LOC[nnP[nnP[k]]] +4.0*dy_LOC[nnP[k]] -3.0*rhoLc );
		// if( ( dy_LOC[nnP[nnP[k]]] - dy_LOC[k] )*dirDrvC_y < 0.0  ){
		// 	dirDrvC_y = 0.5*( dy_LOC[nnP[k]] - dy_LOC[nnM[k]] );
		// }
		dirDrvB = 0.5*( B_LOC[nnP[k]] - B_LOC[nnM[k]] );
		dirDrvC_x = 0.5*( dx_LOC[nnP[k]] - dx_LOC[nnM[k]] );
		dirDrvC_y = 0.5*( dy_LOC[nnP[k]] - dy_LOC[nnM[k]] );
		eD = 1.5*kappa*(dirDrvB - dirDrv*lapRho_LOC[k] - dx_LOC[k]*dirDrvC_x - dy_LOC[k]*dirDrvC_y);

		G = Gconst*rhoLc*( 1.0+2.0*u2 - 3.0*u1 + gravConst );
		degRate = 0.5/(tauVp + (rhoLc - rhoV)*relTauRhoRatio);
		//calculating the equilibirum function:
		gamAux = weight * ( - 1.5*(u1*u1 + u2*u2 ) +3.0*(-u1+u2) + 4.5*(-u1+u2)*(-u1+u2) );
		gamma = weight + gamAux;
		gEQ = 3.0*prssr_LOC[k]*weight + rhoLc*gamAux;

		g_6[nnP[k]] = g+(gEQ - g)*degRate + ( eD - Du_LOC[k] )*gamma + 0.5*( dirDrv - EU )*gamAux + G;
	}
}

void preStr_G7(){

	arrayCopy(distr_Loc, g_7, lenAr);
	nnP = nn_7;
	nnM = nn_5;
	
	fltp weight = w3;
	
	fltp g,gEQ,gamma,gamAux,u1,u2,degRate,rhoLc,eD,EU,dirDrv,dirDrvB,dirDrvC_x,dirDrvC_y,G;

	for(int k=0; k<lenAr; k++){
		g = distr_Loc[k];
		rhoLc = rho_LOC[k];
		u1 = u_1_LOC[k];
		u2 = u_2_LOC[k];
		
		EU = dx_LOC[k]*u1+dy_LOC[k]*u2;
		dirDrv = 0.5*( -rho_LOC[nnP[nnP[k]]] +4.0*rho_LOC[nnP[k]] -3.0*rhoLc );
		if( ( rho_LOC[nnP[nnP[k]]] - rho_LOC[k] )*dirDrv < 0.0  ){
			dirDrv = 0.5*( rho_LOC[nnP[k]] - rho_LOC[nnM[k]] );
		}
		// dirDrvB = 0.5*( -B_LOC[nnP[nnP[k]]] +4.0*B_LOC[nnP[k]] -3.0*rhoLc );
		// if( ( B_LOC[nnP[nnP[k]]] - B_LOC[k] )*dirDrvB < 0.0  ){
		// 	dirDrvB = 0.5*( B_LOC[nnP[k]] - B_LOC[nnM[k]] );
		// }
		// dirDrvC_x = 0.5*( -dx_LOC[nnP[nnP[k]]] +4.0*dx_LOC[nnP[k]] -3.0*rhoLc );
		// if( ( dx_LOC[nnP[nnP[k]]] - dx_LOC[k] )*dirDrvC_x < 0.0  ){
		// 	dirDrvC_x = 0.5*( dx_LOC[nnP[k]] - dx_LOC[nnM[k]] );
		// }
		// dirDrvC_y = 0.5*( -dy_LOC[nnP[nnP[k]]] +4.0*dy_LOC[nnP[k]] -3.0*rhoLc );
		// if( ( dy_LOC[nnP[nnP[k]]] - dy_LOC[k] )*dirDrvC_y < 0.0  ){
		// 	dirDrvC_y = 0.5*( dy_LOC[nnP[k]] - dy_LOC[nnM[k]] );
		// }

		dirDrvB = 0.5*( B_LOC[nnP[k]] - B_LOC[nnM[k]] );
		dirDrvC_x = 0.5*( dx_LOC[nnP[k]] - dx_LOC[nnM[k]] );
		dirDrvC_y = 0.5*( dy_LOC[nnP[k]] - dy_LOC[nnM[k]] );
		eD = 1.5*kappa*(dirDrvB - dirDrv*lapRho_LOC[k] - dx_LOC[k]*dirDrvC_x - dy_LOC[k]*dirDrvC_y);
		
		G = -Gconst*rhoLc*( 1.0 + 4.0*u2 + 3.0*u1 - gravConst );
		degRate = 0.5/(tauVp + (rhoLc - rhoV)*relTauRhoRatio);
		//calculating the equilibirum function:
		gamAux = weight * ( - 1.5*(u1*u1 + u2*u2 ) - 3.0*(u1+u2) + 4.5*(u1+u2)*(u1+u2) );
		gamma = weight + gamAux;
		gEQ = 3.0*prssr_LOC[k]*weight + rhoLc*gamAux;

		g_7[nnP[k]] = g+(gEQ - g)*degRate + ( eD - Du_LOC[k] )*gamma  + 0.5*( dirDrv - EU )*gamAux + G;
	}
}

void preStr_G8(){
			
	arrayCopy(distr_Loc, g_8, lenAr);
	nnP = nn_8;
	nnM = nn_6;
	
	fltp weight = w3;
	
	fltp g,gEQ,gamma,gamAux,u1,u2,degRate,rhoLc,eD,EU,dirDrv,dirDrvB,dirDrvC_x,dirDrvC_y,G;

	for(int k=0; k<lenAr; k++){
		g = distr_Loc[k];
		rhoLc = rho_LOC[k];
		u1 = u_1_LOC[k];
		u2 = u_2_LOC[k];
		
		EU = dx_LOC[k]*u1+dy_LOC[k]*u2;
		dirDrv = 0.5*( -rho_LOC[nnP[nnP[k]]] +4.0*rho_LOC[nnP[k]] -3.0*rhoLc );
		if( ( rho_LOC[nnP[nnP[k]]] - rho_LOC[k] )*dirDrv < 0.0  ){
			dirDrv = 0.5*( rho_LOC[nnP[k]] - rho_LOC[nnM[k]] );
		}
		// dirDrvB = 0.5*( -B_LOC[nnP[nnP[k]]] +4.0*B_LOC[nnP[k]] -3.0*rhoLc );
		// if( ( B_LOC[nnP[nnP[k]]] - B_LOC[k] )*dirDrvB < 0.0  ){
		// 	dirDrvB = 0.5*( B_LOC[nnP[k]] - B_LOC[nnM[k]] );
		// }
		// dirDrvC_x = 0.5*( -dx_LOC[nnP[nnP[k]]] +4.0*dx_LOC[nnP[k]] -3.0*rhoLc );
		// if( ( dx_LOC[nnP[nnP[k]]] - dx_LOC[k] )*dirDrvC_x < 0.0  ){
		// 	dirDrvC_x = 0.5*( dx_LOC[nnP[k]] - dx_LOC[nnM[k]] );
		// }
		// dirDrvC_y = 0.5*( -dy_LOC[nnP[nnP[k]]] +4.0*dy_LOC[nnP[k]] -3.0*rhoLc );
		// if( ( dy_LOC[nnP[nnP[k]]] - dy_LOC[k] )*dirDrvC_y < 0.0  ){
		// 	dirDrvC_y = 0.5*( dy_LOC[nnP[k]] - dy_LOC[nnM[k]] );
		// }

		dirDrvB = 0.5*( B_LOC[nnP[k]] - B_LOC[nnM[k]] );
		dirDrvC_x = 0.5*( dx_LOC[nnP[k]] - dx_LOC[nnM[k]] );
		dirDrvC_y = 0.5*( dy_LOC[nnP[k]] - dy_LOC[nnM[k]] );
		eD = 1.5*kappa*(dirDrvB - dirDrv*lapRho_LOC[k] - dx_LOC[k]*dirDrvC_x - dy_LOC[k]*dirDrvC_y);

		G = -Gconst*rhoLc*( 1.0 + 4.0*u2 - 3.0*u1 - gravConst );
		degRate = 0.5/(tauVp + (rhoLc - rhoV)*relTauRhoRatio);
		//calculating the equilibirum function:
		gamAux = weight * ( - 1.5*(u1*u1 + u2*u2 ) + 3.0*(u1-u2) + 4.5*(u1-u2)*(u1-u2) );
		gamma = weight + gamAux;
		gEQ = 3.0*prssr_LOC[k]*weight + rhoLc*gamAux;

		g_8[nnP[k]] = g+(gEQ - g)*degRate + ( eD - Du_LOC[k] )*gamma  + 0.5*( dirDrv - EU )*gamAux + G;	
	}
}


void preStr_F0(){
	arrayCopy(distr_Loc, f_0, lenAr);
	
	fltp weight,rhoLc,u1,u2,degRate,gamma,H;
	weight = w1;
	for( int k=0; k<lenAr; k++){
		rhoLc = rho_LOC[k];
		u1 = u_1_LOC[k];
		u2 = u_2_LOC[k];
		H = -dx_LOC[k]*u1 -dy_LOC[k]*u2 +3.0*rhoLc*( +dxPsi_LOC[k]*u1 +dyPsi_LOC[k]*u2 );
		gamma = weight * ( 1.0 - 1.5*(u1*u1 + u2*u2 ) );

		degRate = 0.5/(tauVp + (rhoLc - rhoV)*relTauRhoRatio);
		
		f_0[k] = distr_Loc[k] +(rhoLc*gamma - distr_Loc[k])*degRate	+ 0.5*H*gamma ;
	}
}

void preStr_F1(){
	arrayCopy(distr_Loc, f_1, lenAr);
	nnP = nn_1;
	nnM = nn_3;

	fltp weight,rhoLc,u1,u2,degRate,gamma,dirDrv,dirDrvPsi,H;
	weight = w2;
	
	for( int k=0; k<lenAr; k++){
		rhoLc = rho_LOC[k];
		u1 = u_1_LOC[k];
		u2 = u_2_LOC[k];
		dirDrv = 0.5*( -rho_LOC[nnP[nnP[k]]] +4.0*rho_LOC[nnP[k]] -3.0*rhoLc );
		if( ( rho_LOC[nnP[nnP[k]]] - rho_LOC[k] )*dirDrv < 0.0  ){
			dirDrv = 0.5*( rho_LOC[nnP[k]] - rho_LOC[nnM[k]] );
		}
		// dirDrvPsi = 0.5*( -psi_LOC[nnP[nnP[k]]] +4.0*psi_LOC[nnP[k]] -3.0*psi_LOC[k] );
		// if( ( psi_LOC[nnP[nnP[k]]] - psi_LOC[k] )*dirDrvPsi < 0.0  ){
		// 	dirDrvPsi = 0.5*( psi_LOC[nnP[k]] - psi_LOC[nnM[k]] );
		// }

		dirDrvPsi = 0.5*( psi_LOC[nnP[k]] - psi_LOC[nnM[k]] );

		H = dirDrv - dx_LOC[k]*u1 - dy_LOC[k]*u2 -3.0*rhoLc*( dirDrvPsi - dxPsi_LOC[k]*u1 - dyPsi_LOC[k]*u2 );
		gamma = weight * ( 1.0 - 1.5*(u1*u1 + u2*u2 ) + 3.0*u1 + 4.5*u1*u1 );

		degRate = 0.5/(tauVp + (rhoLc - rhoV)*relTauRhoRatio);

		f_1[nnP[k]] = distr_Loc[k] +(rhoLc*gamma - distr_Loc[k])*degRate + 0.5*H*gamma 	;
	}
}

void preStr_F2(){
	arrayCopy(distr_Loc, f_2, lenAr);
	nnP = nn_2;
	nnM = nn_4;
	
	fltp weight,rhoLc,u1,u2,degRate,gamma,dirDrv,dirDrvPsi,H;
	weight = w2;
	
	for( int k=0; k<lenAr; k++){
		rhoLc = rho_LOC[k];
		u1 = u_1_LOC[k];
		u2 = u_2_LOC[k];
		dirDrv = 0.5*( -rho_LOC[nnP[nnP[k]]] +4.0*rho_LOC[nnP[k]] -3.0*rhoLc );
		if( ( rho_LOC[nnP[nnP[k]]] - rho_LOC[k] )*dirDrv < 0.0  ){
			dirDrv = 0.5*( rho_LOC[nnP[k]] - rho_LOC[nnM[k]] );
		}
		// dirDrvPsi = 0.5*( -psi_LOC[nnP[nnP[k]]] +4.0*psi_LOC[nnP[k]] -3.0*psi_LOC[k] );
		// if( ( psi_LOC[nnP[nnP[k]]] - psi_LOC[k] )*dirDrvPsi < 0.0  ){
		// 	dirDrvPsi = 0.5*( psi_LOC[nnP[k]] - psi_LOC[nnM[k]] );
		// }

		dirDrvPsi = 0.5*( psi_LOC[nnP[k]] - psi_LOC[nnM[k]] );

		H = dirDrv - dx_LOC[k]*u1 - dy_LOC[k]*u2 -3.0*rhoLc*( dirDrvPsi - dxPsi_LOC[k]*u1 - dyPsi_LOC[k]*u2 );
		gamma = weight * ( 1.0 - 1.5*(u1*u1 + u2*u2 )  + 3.0*u2 + 4.5*u2*u2 );

		degRate = 0.5/(tauVp + (rhoLc - rhoV)*relTauRhoRatio);

		f_2[nnP[k]] = distr_Loc[k] +(rhoLc*gamma - distr_Loc[k])*degRate + 0.5*H*gamma 	;
	}
}

void preStr_F3(){
	arrayCopy(distr_Loc, f_3, lenAr);
	nnP = nn_3;
	nnM = nn_1;
	
	fltp weight,rhoLc,u1,u2,degRate,gamma,dirDrv,dirDrvPsi,H;
	weight = w2;
	
	for( int k=0; k<lenAr; k++){
		rhoLc = rho_LOC[k];
		u1 = u_1_LOC[k];
		u2 = u_2_LOC[k];
		dirDrv = 0.5*( -rho_LOC[nnP[nnP[k]]] +4.0*rho_LOC[nnP[k]] -3.0*rhoLc );
		if( ( rho_LOC[nnP[nnP[k]]] - rho_LOC[k] )*dirDrv < 0.0  ){
			dirDrv = 0.5*( rho_LOC[nnP[k]] - rho_LOC[nnM[k]] );
		}
		// dirDrvPsi = 0.5*( -psi_LOC[nnP[nnP[k]]] +4.0*psi_LOC[nnP[k]] -3.0*psi_LOC[k] );
		// if( ( psi_LOC[nnP[nnP[k]]] - psi_LOC[k] )*dirDrvPsi < 0.0  ){
		// 	dirDrvPsi = 0.5*( psi_LOC[nnP[k]] - psi_LOC[nnM[k]] );
		// }

		dirDrvPsi = 0.5*( psi_LOC[nnP[k]] - psi_LOC[nnM[k]] );

		H = dirDrv - dx_LOC[k]*u1 - dy_LOC[k]*u2 -3.0*rhoLc*( dirDrvPsi - dxPsi_LOC[k]*u1 - dyPsi_LOC[k]*u2 );
		gamma = weight * ( 1.0 - 1.5*(u1*u1 + u2*u2 ) -3.0*u1 + 4.5*u1*u1 );

		degRate = 0.5/(tauVp + (rhoLc - rhoV)*relTauRhoRatio);

		f_3[nnP[k]] = distr_Loc[k] +(rhoLc*gamma - distr_Loc[k])*degRate + 0.5*H*gamma 	;
	}
}

void preStr_F4(){
	arrayCopy(distr_Loc, f_4, lenAr);
	nnP = nn_4;
	nnM = nn_2;
	
	fltp weight,rhoLc,u1,u2,degRate,gamma,dirDrv,dirDrvPsi,H;
	weight = w2;
	
	for( int k=0; k<lenAr; k++){
		rhoLc = rho_LOC[k];
		u1 = u_1_LOC[k];
		u2 = u_2_LOC[k];
		dirDrv = 0.5*( -rho_LOC[nnP[nnP[k]]] +4.0*rho_LOC[nnP[k]] -3.0*rhoLc );
		if( ( rho_LOC[nnP[nnP[k]]] - rho_LOC[k] )*dirDrv < 0.0  ){
			dirDrv = 0.5*( rho_LOC[nnP[k]] - rho_LOC[nnM[k]] );
		}
		// dirDrvPsi = 0.5*( -psi_LOC[nnP[nnP[k]]] +4.0*psi_LOC[nnP[k]] -3.0*psi_LOC[k] );
		// if( ( psi_LOC[nnP[nnP[k]]] - psi_LOC[k] )*dirDrvPsi < 0.0  ){
		// 	dirDrvPsi = 0.5*( psi_LOC[nnP[k]] - psi_LOC[nnM[k]] );
		// }

		dirDrvPsi = 0.5*( psi_LOC[nnP[k]] - psi_LOC[nnM[k]] );

		H = dirDrv - dx_LOC[k]*u1 - dy_LOC[k]*u2 -3.0*rhoLc*( dirDrvPsi - dxPsi_LOC[k]*u1 - dyPsi_LOC[k]*u2 );
		gamma = weight * ( 1.0 - 1.5*(u1*u1 + u2*u2 ) -3.0*u2 + 4.5*u2*u2 );

		degRate = 0.5/(tauVp + (rhoLc - rhoV)*relTauRhoRatio);

		f_4[nnP[k]] = distr_Loc[k] +(rhoLc*gamma - distr_Loc[k])*degRate + 0.5*H*gamma 	;
	}
}

void preStr_F5(){
	arrayCopy(distr_Loc, f_5, lenAr);
	nnP = nn_5;
	nnM = nn_7;
	
	fltp weight,rhoLc,u1,u2,degRate,gamma,dirDrv,dirDrvPsi,H;
	weight = w3;
	
	for( int k=0; k<lenAr; k++){
		rhoLc = rho_LOC[k];
		u1 = u_1_LOC[k];
		u2 = u_2_LOC[k];
		dirDrv = 0.5*( -rho_LOC[nnP[nnP[k]]] +4.0*rho_LOC[nnP[k]] -3.0*rhoLc );
		if( ( rho_LOC[nnP[nnP[k]]] - rho_LOC[k] )*dirDrv < 0.0  ){
			dirDrv = 0.5*( rho_LOC[nnP[k]] - rho_LOC[nnM[k]] );
		}
		// dirDrvPsi = 0.5*( -psi_LOC[nnP[nnP[k]]] +4.0*psi_LOC[nnP[k]] -3.0*psi_LOC[k] );
		// if( ( psi_LOC[nnP[nnP[k]]] - psi_LOC[k] )*dirDrvPsi < 0.0  ){
		// 	dirDrvPsi = 0.5*( psi_LOC[nnP[k]] - psi_LOC[nnM[k]] );
		// }

		dirDrvPsi = 0.5*( psi_LOC[nnP[k]] - psi_LOC[nnM[k]] );

		H = dirDrv - dx_LOC[k]*u1 - dy_LOC[k]*u2 -3.0*rhoLc*( dirDrvPsi - dxPsi_LOC[k]*u1 - dyPsi_LOC[k]*u2 );
		gamma = weight * ( 1.0 - 1.5*(u1*u1 + u2*u2 ) + 3.0*(u1+u2) + 4.5*(u1+u2)*(u1+u2) );

		degRate = 0.5/(tauVp + (rhoLc - rhoV)*relTauRhoRatio);

		f_5[nnP[k]] = distr_Loc[k] +(rhoLc*gamma - distr_Loc[k])*degRate + 0.5*H*gamma ;
	}
}

void preStr_F6(){
	arrayCopy(distr_Loc, f_6, lenAr);
	nnP = nn_6;
	nnM = nn_8;
	
	fltp weight,rhoLc,u1,u2,degRate,gamma,dirDrv,dirDrvPsi,H;
	weight = w3;
	
	for( int k=0; k<lenAr; k++){
		rhoLc = rho_LOC[k];
		u1 = u_1_LOC[k];
		u2 = u_2_LOC[k];
		dirDrv = 0.5*( -rho_LOC[nnP[nnP[k]]] +4.0*rho_LOC[nnP[k]] -3.0*rhoLc );
		if( ( rho_LOC[nnP[nnP[k]]] - rho_LOC[k] )*dirDrv < 0.0  ){
			dirDrv = 0.5*( rho_LOC[nnP[k]] - rho_LOC[nnM[k]] );
		}
		// dirDrvPsi = 0.5*( -psi_LOC[nnP[nnP[k]]] +4.0*psi_LOC[nnP[k]] -3.0*psi_LOC[k] );
		// if( ( psi_LOC[nnP[nnP[k]]] - psi_LOC[k] )*dirDrvPsi < 0.0  ){
		// 	dirDrvPsi = 0.5*( psi_LOC[nnP[k]] - psi_LOC[nnM[k]] );
		// }

		dirDrvPsi = 0.5*( psi_LOC[nnP[k]] - psi_LOC[nnM[k]] );

		H = dirDrv - dx_LOC[k]*u1 - dy_LOC[k]*u2 -3.0*rhoLc*( dirDrvPsi - dxPsi_LOC[k]*u1 - dyPsi_LOC[k]*u2 );
		gamma = weight * ( 1.0 - 1.5*(u1*u1 + u2*u2 ) +3.0*(-u1+u2) + 4.5*(-u1+u2)*(-u1+u2) );

		degRate = 0.5/(tauVp + (rhoLc - rhoV)*relTauRhoRatio);

		f_6[nnP[k]] = distr_Loc[k] +(rhoLc*gamma - distr_Loc[k])*degRate + 0.5*H*gamma ;
	}
}

void preStr_F7(){
	arrayCopy(distr_Loc, f_7, lenAr);
	nnP = nn_7;
	nnM = nn_5;
	
	fltp weight,rhoLc,u1,u2,degRate,gamma,dirDrv,dirDrvPsi,H;
	weight = w3;
	
	for( int k=0; k<lenAr; k++){
		rhoLc = rho_LOC[k];
		u1 = u_1_LOC[k];
		u2 = u_2_LOC[k];
		dirDrv = 0.5*( -rho_LOC[nnP[nnP[k]]] +4.0*rho_LOC[nnP[k]] -3.0*rhoLc );
		if( ( rho_LOC[nnP[nnP[k]]] - rho_LOC[k] )*dirDrv < 0.0  ){
			dirDrv = 0.5*( rho_LOC[nnP[k]] - rho_LOC[nnM[k]] );
		}
		// dirDrvPsi = 0.5*( -psi_LOC[nnP[nnP[k]]] +4.0*psi_LOC[nnP[k]] -3.0*psi_LOC[k] );
		// if( ( psi_LOC[nnP[nnP[k]]] - psi_LOC[k] )*dirDrvPsi < 0.0  ){
		// 	dirDrvPsi = 0.5*( psi_LOC[nnP[k]] - psi_LOC[nnM[k]] );
		// }

		dirDrvPsi = 0.5*( psi_LOC[nnP[k]] - psi_LOC[nnM[k]] );

		H = dirDrv - dx_LOC[k]*u1 - dy_LOC[k]*u2 -3.0*rhoLc*( dirDrvPsi - dxPsi_LOC[k]*u1 - dyPsi_LOC[k]*u2 );
		gamma = weight * ( 1.0 - 1.5*(u1*u1 + u2*u2 ) - 3.0*(u1+u2) + 4.5*(u1+u2)*(u1+u2) );

		degRate = 0.5/(tauVp + (rhoLc - rhoV)*relTauRhoRatio);

		f_7[nnP[k]] = distr_Loc[k] +(rhoLc*gamma - distr_Loc[k])*degRate + 0.5*H*gamma 	;
	}
}

void preStr_F8(){
	arrayCopy(distr_Loc, f_8, lenAr);
	nnP = nn_8;
	nnM = nn_6;
	
	fltp weight,rhoLc,u1,u2,degRate,gamma,dirDrv,dirDrvPsi,H;
	weight = w3;
	
	for( int k=0; k<lenAr; k++){
		rhoLc = rho_LOC[k];
		u1 = u_1_LOC[k];
		u2 = u_2_LOC[k];
		dirDrv = 0.5*( -rho_LOC[nnP[nnP[k]]] +4.0*rho_LOC[nnP[k]] -3.0*rhoLc );
		if( ( rho_LOC[nnP[nnP[k]]] - rho_LOC[k] )*dirDrv < 0.0  ){
			dirDrv = 0.5*( rho_LOC[nnP[k]] - rho_LOC[nnM[k]] );
		}
		// dirDrvPsi = 0.5*( -psi_LOC[nnP[nnP[k]]] +4.0*psi_LOC[nnP[k]] -3.0*psi_LOC[k] );
		// if( ( psi_LOC[nnP[nnP[k]]] - psi_LOC[k] )*dirDrvPsi < 0.0  ){
		// 	dirDrvPsi = 0.5*( psi_LOC[nnP[k]] - psi_LOC[nnM[k]] );
		// }

		dirDrvPsi = 0.5*( psi_LOC[nnP[k]] - psi_LOC[nnM[k]] );
		
		H = dirDrv - dx_LOC[k]*u1 - dy_LOC[k]*u2 -3.0*rhoLc*( dirDrvPsi - dxPsi_LOC[k]*u1 - dyPsi_LOC[k]*u2 );
		gamma = weight * ( 1.0 - 1.5*(u1*u1 + u2*u2 ) + 3.0*(u1-u2) + 4.5*(u1-u2)*(u1-u2) );

		degRate = 0.5/(tauVp + (rhoLc - rhoV)*relTauRhoRatio);

		f_8[nnP[k]] = distr_Loc[k] +(rhoLc*gamma - distr_Loc[k])*degRate + 0.5*H*gamma 	;
	}
}
























